package nl.avans.ivh11.Facturering;

import nl.avans.ivh11.Facturering.controller.CustomerController;
import nl.avans.ivh11.Facturering.domain.customer.Customer;
import nl.avans.ivh11.Facturering.domain.customer.Name;
import nl.avans.ivh11.Facturering.service.CustomerService;
import nl.avans.ivh11.Facturering.service.InsuranceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CustomerControllerTests {

    private MockMvc mockMvc;

    @Autowired
    @Mock
    private CustomerService customerServiceMock;

    @Autowired
    @Mock
    private InsuranceService insuranceServiceMock;

    @InjectMocks
    private CustomerController customerController;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        this.customerServiceMock = Mockito.mock(CustomerService.class);
        mockMvc = MockMvcBuilders.standaloneSetup(new CustomerController(customerServiceMock, insuranceServiceMock))
                .build();
    }

    @Test
    public void Customer_findById_TodoEntryNotFound_ShouldNotBeFound() throws Exception {
      // when(customerServiceMock.findById(1L)).thenThrow(new CustomerNotFoundException(""));

        mockMvc.perform(get("/customer/notfound/{id}", 1L))
                .andExpect(status().isNotFound());

        //verify(customerServiceMock, times(1)).findById(1L);
    }

    @Test
    public void Customer_findById_EntryFound() throws Exception {
        Customer found = new Customer() {{
                    setId(1L);
                    setName(new Name("Lorem", "Ipsum"));
        }};

        ArrayList<Customer> cList = new ArrayList<>();
        cList.add(found);

        when(customerServiceMock.getCustomers()).thenReturn(cList);

        mockMvc.perform(get("/customer/", 1L))
                .andExpect(status().isOk());
                //.andExpect(model().attribute("customers", hasProperty("size", is(1))));
                //.andExpect(model().attribute("todo", hasProperty("name", is("Lorem ipsum"))));

        verify(customerServiceMock, times(1)).getCustomers();
        verifyNoMoreInteractions(customerServiceMock);
    }
}

