package nl.avans.ivh11.Facturering;

import nl.avans.ivh11.Facturering.controller.CustomerController;
import nl.avans.ivh11.Facturering.domain.customer.Customer;
import nl.avans.ivh11.Facturering.domain.customer.Name;
import nl.avans.ivh11.Facturering.repository.AddressRepository;
import nl.avans.ivh11.Facturering.repository.CustomerRepository;
import nl.avans.ivh11.Facturering.repository.InsuranceRepository;
import nl.avans.ivh11.Facturering.repository.NameRepository;
import nl.avans.ivh11.Facturering.service.CustomerService;
import static org.junit.Assert.*;

import nl.avans.ivh11.Facturering.service.InsuranceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CustomerServiceTests {

    @Autowired
    private CustomerService customerService;

    @Autowired
    @Mock
    private CustomerRepository customerRepository;

    @Autowired
    @Mock
    private AddressRepository addressRepository;

    @Autowired
    @Mock
    private NameRepository nameRepository;

    @Autowired
    @Mock
    private InsuranceRepository insuranceRepository;

    @Autowired
    @Mock
    private InsuranceService insuranceService;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);

        Customer lorem = new Customer() {{
            setId(20L);
            setName(new Name("Lorem", "Ipsum"));
        }};

        this.customerRepository = Mockito.mock(CustomerRepository.class);
        this.customerService = new CustomerService(this.customerRepository, this.addressRepository, this.nameRepository, insuranceRepository, insuranceService);

        Mockito.when(customerRepository.findOne(20L))
                .thenReturn(lorem);
    }

    @Test
    public void CustomerService_findById_EntryShouldBeFound() throws Exception {
        String name = "Lorem";
        Customer found = customerService.findById(20L);

        assertEquals((found.getName().getFirstName()), name);
    }

    @Test
    public void CustomerService_findById_EntryShouldNotBeFound() throws Exception {
        String name = "Lorem";
        Customer found = customerService.findById(200000L);

        assertEquals((found), null);
    }
}
