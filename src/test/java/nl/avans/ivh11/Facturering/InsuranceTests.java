package nl.avans.ivh11.Facturering;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.domain.insurance.BasicInsurance;
import nl.avans.ivh11.Facturering.domain.insurance.Insurance;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class InsuranceTests {

    Insurance insurance;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        insurance = new BasicInsurance();

        Treatment t1 = new Treatment("1281", "Controle ", new BigDecimal("1"),
                "15", new BigDecimal("120"));
        Treatment t2 = new Treatment("1282", "Vulling ", new BigDecimal("1"),
                "30", new BigDecimal("160"));

        List<Treatment> tList = new ArrayList<Treatment>();
        tList.add(t1);
        tList.add(t2);
        insurance.addIncludedTreatments(tList);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void Insurance_TestIncludedTreatments_IndexNotFound() throws Exception {

        List<Treatment> tList = insurance.getIncludedTreatments();
        tList.get(10);
    }

    @Test
    public void Insurance_TestIncludedTreatments() throws Exception {

        List<Treatment> tList = insurance.getIncludedTreatments();

        assertEquals(tList.size(), 2);
        assertEquals(tList.get(0).getName(), "Controle ");
    }
}
