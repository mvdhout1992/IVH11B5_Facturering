package nl.avans.ivh11.Facturering;

import nl.avans.ivh11.Facturering.domain.customer.Customer;
import nl.avans.ivh11.Facturering.domain.customer.Name;
import nl.avans.ivh11.Facturering.repository.CustomerRepository;
import nl.avans.ivh11.Facturering.service.CustomerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CustomerRepositoryTests {

    @Autowired
    @Mock
    private CustomerRepository customerRepository;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);

        Customer lorem = new Customer() {{
            setId(20L);
            setName(new Name("Lorem", "Ipsum"));
        }};

        Customer lorem2 = new Customer() {{
            setId(30L);
            setName(new Name("Lorem2", "Ipsum"));
        }};

        ArrayList<Customer> cList = new ArrayList<>();
        cList.add(lorem);
        cList.add(lorem2);

        this.customerRepository = Mockito.mock(CustomerRepository.class);

        Mockito.when(customerRepository.findAll())
                .thenReturn(cList);

        Mockito.when(customerRepository.exists(20L))
                .thenReturn(true);

        Mockito.when(customerRepository.exists(30L))
                .thenReturn(true);
    }

    @Test
    public void CustomerService_findById_EntryShouldBeFound() throws Exception {

        List<Customer> cList = (ArrayList<Customer>)customerRepository.findAll();

        assertEquals(cList.size(), 2);
        assertEquals(customerRepository.exists(20L), true);
        assertEquals(customerRepository.exists(30L), true);
        assertEquals(customerRepository.exists(50L), false);
    }
}
