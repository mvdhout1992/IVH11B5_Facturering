package nl.avans.ivh11.Facturering.service;


import nl.avans.ivh11.Facturering.domain.treatment.Memento.TreatmentMemento;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.repository.MementoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class TreatmentMementoService {
    @Autowired
    private final MementoRepository mementoRepository;

    public TreatmentMementoService(MementoRepository mementoRepository) {
        this.mementoRepository = mementoRepository;
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public ArrayList<TreatmentMemento> getTreatments() {
        return (ArrayList<TreatmentMemento>) this.mementoRepository.findAll();
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public TreatmentMemento createTrementMemento(TreatmentMemento treatment) {
        return this.mementoRepository.save(treatment);
    }

    public TreatmentMemento getTreamentMemento(Treatment treatment){
        for(TreatmentMemento memento : mementoRepository.findAll()){
            if(memento.getSavedTreatment().equals(treatment)){
                return memento ;
            }
        }
        return createTrementMemento(new TreatmentMemento(treatment));
    }
}
