package nl.avans.ivh11.Facturering.service;

import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.DiscountStrategy;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.domain.insurance.Insurance;
import nl.avans.ivh11.Facturering.domain.insurance.BasicInsurance;
import nl.avans.ivh11.Facturering.domain.insurance.SupplementaryInsurance;
import nl.avans.ivh11.Facturering.domain.insurance.decorator.DentalPackageDecorator;
import nl.avans.ivh11.Facturering.domain.insurance.decorator.FysioPackageDecorator;
import nl.avans.ivh11.Facturering.domain.insurance.decorator.InsurancePackageDecorator;
import nl.avans.ivh11.Facturering.repository.DiscountStrategyRepository;
import nl.avans.ivh11.Facturering.repository.InsuranceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class InsuranceService {

    @Autowired
    private final InsuranceRepository insuranceRepository;

    @Autowired
    private final DiscountStrategyRepository discountStrategyRepository;

    @Autowired
    private final TreatmentService treatmentService;

    private List<Treatment> basicPackage;
    private List<Treatment> supplementaryPackage;
    private List<Treatment> dentalPackage;
    private List<Treatment> fysioPackage;

    public InsuranceService(InsuranceRepository repository, DiscountStrategyRepository discountStrategyRepository, TreatmentService treatmentService) {
        this.insuranceRepository = repository;
        this.treatmentService = treatmentService;
        this.discountStrategyRepository = discountStrategyRepository;
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public ArrayList<Insurance> getInsurances() {
        return (ArrayList<Insurance>) this.insuranceRepository.findAll();
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public ArrayList<Insurance> getBaseInsurances() {
        List<Insurance> list = (ArrayList<Insurance>)this.insuranceRepository.findAll();
        ArrayList<Insurance> out = new ArrayList<>();

        for(Insurance i : list){
            if (i.isBase){
                out.add(i);
            }
        }

        /*
        Insurance i1 = this.insuranceRepository.findOne(1l);
        Insurance i2 = this.insuranceRepository.findOne(2l);
        ArrayList<Insurance> out = new ArrayList<Insurance>(){{
            add(i1);
            add(i2);
        }};
        */

        return out;
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public ArrayList<DiscountStrategy> getDiscounts() {
        return (ArrayList<DiscountStrategy>) this.discountStrategyRepository.findAll();
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public Insurance createInsurance(Insurance insurance) {
        this.setPackages();
        if (insurance instanceof InsurancePackageDecorator){
            Insurance i = ((InsurancePackageDecorator) insurance).getInsurance();
            i = this.setBasePackages(i);
            i.hasPackages = true;
            i.isBase = false;

            if (insurance instanceof DentalPackageDecorator)
                i.addIncludedTreatments(dentalPackage);

            if (insurance instanceof FysioPackageDecorator)
                i.addIncludedTreatments(fysioPackage);

            insurance = i;
        }
        else
            insurance = this.setBasePackages(insurance);

        return this.insuranceRepository.save(insurance);
    }

    private Insurance setBasePackages(Insurance insurance){
        String name = insurance.getName();

        if (name.matches("Basis verzekering"))
            insurance.setIncludedTreatments(basicPackage);

        if (name.matches("Aanvullende verzekering"))
            insurance.setIncludedTreatments(supplementaryPackage);

        return insurance;
    }

    private void setPackages(){
        Treatment t1 = treatmentService.getTreatmentByCode("30");
        Treatment t2 = treatmentService.getTreatmentByCode("45");
        Treatment t3 = treatmentService.getTreatmentByCode("54");

        this.basicPackage = new ArrayList<Treatment>(){{
            add(t1);
            add(t2);
            add(t3);
        }};

        Treatment t4 = treatmentService.getTreatmentByCode("47");
        Treatment t5 = treatmentService.getTreatmentByCode("34");
        Treatment t6 = treatmentService.getTreatmentByCode("35");

        this.supplementaryPackage = new ArrayList<Treatment>(){{
            addAll(basicPackage);
            add(t4);
            add(t5);
            add(t6);
        }};

        Treatment t7 = treatmentService.getTreatmentByCode("61");
        Treatment t8 = treatmentService.getTreatmentByCode("65");
        Treatment t9 = treatmentService.getTreatmentByCode("66");

        this.fysioPackage = new ArrayList<Treatment>(){{
            add(t7);
            add(t8);
            add(t9);
        }};

        Treatment t10 = treatmentService.getTreatmentByCode("81");
        Treatment t11 = treatmentService.getTreatmentByCode("82");
        Treatment t12 = treatmentService.getTreatmentByCode("83");

        this.dentalPackage = new ArrayList<Treatment>(){{
            add(t10);
            add(t11);
            add(t12);
        }};
    }
}
