package nl.avans.ivh11.Facturering.service;



import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.repository.TreatmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class TreatmentService {

    @Autowired
    private final TreatmentRepository treatmentRepository;

    public TreatmentService(TreatmentRepository repository) {
        this.treatmentRepository = repository;
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public ArrayList<Treatment> getTreatments() {
        return (ArrayList<Treatment>) this.treatmentRepository.findAll();
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public Treatment getTreatmentByName(String name) {
        return this.treatmentRepository.getTreatmentByName(name);
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public Treatment getTreatmentByCode(String code) {
        return this.treatmentRepository.getTreatmentByCode(code);
    }
    public Treatment getTreatmentById(Long id) { return this.treatmentRepository.findOne(id);}
    public void updateTreatment(Treatment treat){  this.treatmentRepository.save(treat);}
//    public Treatment getTreatmentByCode(String code) {
//        int num = Integer.parseInt(code);
//        return this.treatmentRepository.getTreatmentByCode(num);
//    }
    public void removeTreatmentById(Long id){
         this.treatmentRepository.delete(id);
    }
    public void deleteAllTreatments(){
        this.treatmentRepository.deleteAll();

    }



    @Transactional(isolation=Isolation.SERIALIZABLE)
    public Treatment createTreatment(Treatment treatment) {
      return this.treatmentRepository.save(treatment);
    }
}
