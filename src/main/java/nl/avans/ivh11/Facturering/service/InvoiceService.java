package nl.avans.ivh11.Facturering.service;

import nl.avans.ivh11.Facturering.domain.invoice.Invoice;
import nl.avans.ivh11.Facturering.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import nl.avans.ivh11.Facturering.domain.invoice.observer.InvoiceEmailObserver;
import nl.avans.ivh11.Facturering.domain.invoice.observer.InvoiceLogObserver;

@Service
public class InvoiceService {

    @Autowired
    private final InvoiceRepository invoiceRepository;

    public InvoiceService(InvoiceRepository repository) {
        this.invoiceRepository = repository;
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public ArrayList<Invoice> getInvoices() {
        ArrayList<Invoice> invoices = (ArrayList<Invoice>) this.invoiceRepository.findAll();
        
        for(Invoice invoice : invoices) {
            addObservers(invoice);
        }
        
        return invoices;
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public Invoice createInvoice(Invoice invoice) {
        addObservers(invoice);
        
        return this.invoiceRepository.save(invoice);
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public Invoice getInvoice(Long id) {
        return invoiceRepository.findOne(id);
    }
    
    public void updateTreatment(Invoice invoice) { 
        this.invoiceRepository.save(invoice);
    }
    
    public void addObservers(Invoice invoice) {
        new InvoiceEmailObserver(invoice);
        new InvoiceLogObserver(invoice);
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public Invoice createOrUpdateInvoice(Invoice invoice) {        
        if (invoice.getId() != null && this.invoiceRepository.exists((invoice.getId()))) {
            return this.updateInvoice(invoice);
        }
        else {
            addObservers(invoice);
            return this.invoiceRepository.save(invoice);
        }
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public Invoice updateInvoice(Invoice invoice) {
        addObservers(invoice);
        
        Invoice old = this.invoiceRepository.findOne(invoice.getId());

        invoice.setId(old.getId());

        return this.invoiceRepository.save(invoice);
    }
}
