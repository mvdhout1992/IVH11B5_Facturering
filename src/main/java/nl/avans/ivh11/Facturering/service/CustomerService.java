package nl.avans.ivh11.Facturering.service;

import nl.avans.ivh11.Facturering.domain.customer.Customer;
import nl.avans.ivh11.Facturering.domain.insurance.Insurance;
import nl.avans.ivh11.Facturering.domain.insurance.decorator.DentalPackageDecorator;
import nl.avans.ivh11.Facturering.domain.insurance.decorator.FysioPackageDecorator;
import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.DiscountStrategy;
import nl.avans.ivh11.Facturering.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private final CustomerRepository customerRepository;

    @Autowired
    private final NameRepository nameRepository;

    @Autowired
    private final AddressRepository addressRepository;

    @Autowired
    private final InsuranceRepository insuranceRepository;

    @Autowired
    private final InsuranceService insuranceService;

    public CustomerService(
            CustomerRepository repository,
            AddressRepository addressRepository,
            NameRepository nameRepository,
            InsuranceRepository insuranceRepository,
            InsuranceService insuranceService) {

        this.customerRepository = repository;
        this.addressRepository = addressRepository;
        this.nameRepository = nameRepository;
        this.insuranceRepository = insuranceRepository;
        this.insuranceService = insuranceService;
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public ArrayList<Customer> getCustomers() {
        return (ArrayList<Customer>) this.customerRepository.findAll();
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public Customer createCustomer(Customer customer) {

        long id = customer.getInsurance().getId();
        Insurance i = insuranceRepository.findOne(id);
        Insurance insurance = new Insurance(i.getName(), i.getInsuranceCompany(), customer.getInsurance().getDiscountStrategy());

        if(customer.dentalPackage){
            DentalPackageDecorator dpd = new DentalPackageDecorator(insurance);
            customer.setInsurance(insuranceService.createInsurance(dpd));
            // Insurance dpd = new DentalPackageDecorator(insurance).getInsurance();
            // customer.setInsurance(insuranceService.createInsurance(dpd));
        }

        if (customer.fysioPackage){
            FysioPackageDecorator fpd = new FysioPackageDecorator(insurance);
            customer.setInsurance(insuranceService.createInsurance(fpd));
        }

        nameRepository.save(customer.getName());
        addressRepository.save(customer.getAddress());
        return this.customerRepository.save(customer);
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public Customer findById(Long id) {
        return this.customerRepository.findOne(id);
    }
}
