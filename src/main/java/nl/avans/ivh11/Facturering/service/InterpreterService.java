package nl.avans.ivh11.Facturering.service;

import nl.avans.ivh11.Facturering.domain.interpreter.*;
import nl.avans.ivh11.Facturering.domain.interpreter.*;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class InterpreterService {
    public InterpreterService() {
    }

    public Expression parse(String query){
        List<String> split = Arrays.asList(query.split("!"));

        return parseParts(split, 1);
    }

    public Expression parseParts(List<String> parts, int index) {

        String currStr = null;
        if (parts.size() > index) {
            currStr = parts.get(index);
        }
        String prevStr = parts.get(index - 1);

        if (parts.size() > index && currStr.startsWith("AND:")) {
            // remove AND: part
            parts.set(index, currStr.substring(4));

            AttributeValue left = getAttributeValueFromQueryStringPart(prevStr);
            return new AndExpression(new TerminalExpression(left), parseParts(parts, index + 1));
        }
        else if(parts.size() > index && currStr.startsWith("OR:")) {
            // remove OR: part
            parts.set(index, currStr.substring(3));

            AttributeValue left = getAttributeValueFromQueryStringPart(prevStr);
            return new OrExpression(new TerminalExpression(left), parseParts(parts, index + 1));
        }
        AttributeValue left = getAttributeValueFromQueryStringPart(prevStr);
        return new TerminalExpression(left);
    }

    public AttributeValue getAttributeValueFromQueryStringPart(String queryPart) {
        String[] split = queryPart.split("=");

        return new AttributeValue(split[0], split[1]);
    }

    public void debugPrintExpressionTree(Expression expr, int level) {
        if (AndExpression.class == expr.getClass() ) {
            AndExpression and = (AndExpression) expr;
            System.out.println(and.toString());
            debugPrintExpressionTree(and.getExpr2(), level++);
        }
        else if (OrExpression.class == expr.getClass() ) {
            OrExpression or = (OrExpression) expr;
            System.out.println(or.toString());
            debugPrintExpressionTree(or.getExpr2(), level++);
        }
        else if (TerminalExpression.class == expr.getClass()) {
            TerminalExpression term = (TerminalExpression) expr;
            System.out.println(term.toString());
        }

    }
}
