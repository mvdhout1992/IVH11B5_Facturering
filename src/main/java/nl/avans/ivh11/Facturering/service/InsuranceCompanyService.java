package nl.avans.ivh11.Facturering.service;

import nl.avans.ivh11.Facturering.domain.insurancecompany.InsuranceCompany;
import nl.avans.ivh11.Facturering.repository.InsuranceCompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class InsuranceCompanyService {
    @Autowired
    private final InsuranceCompanyRepository insuranceCompanyRepository;

    public InsuranceCompanyService(InsuranceCompanyRepository repository) {
        this.insuranceCompanyRepository = repository;
    }

    public ArrayList<InsuranceCompany> getInsuranceCompanies() {
        return (ArrayList<InsuranceCompany>) this.insuranceCompanyRepository.findAll();
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public InsuranceCompany createInsuranceCompany(InsuranceCompany insuranceCompany) {
        return this.insuranceCompanyRepository.save(insuranceCompany);
    }
}
