package nl.avans.ivh11.Facturering.service;


import nl.avans.ivh11.Facturering.domain.treatment.Memento.TreatmentCaretaker;
import nl.avans.ivh11.Facturering.repository.CaretakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class TreatmentCaretakerService {
    @Autowired
    private final CaretakerRepository caretakerRepository;

    public TreatmentCaretakerService(CaretakerRepository caretakerRepository) {
        this.caretakerRepository = caretakerRepository;
    }

    @Transactional(isolation= Isolation.SERIALIZABLE)
    public ArrayList<TreatmentCaretaker> getCaretaker() {
        return (ArrayList<TreatmentCaretaker>) this.caretakerRepository.findAll();
    }

    @Transactional(isolation=Isolation.SERIALIZABLE)
    public TreatmentCaretaker createCareTaker(TreatmentCaretaker caretaker) {
        return this.caretakerRepository.save(caretaker);
    }

//    public TreatmentMemento getTreatmentMemento(int index){
//        for(TreatmentCaretaker caretaker : getCaretaker()){
//            caretaker.getMemento()
//        }
//    }
}
