package nl.avans.ivh11.Facturering.controller;



import nl.avans.ivh11.Facturering.service.TreatmentCaretakerService;
import nl.avans.ivh11.Facturering.service.TreatmentMementoService;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;

import nl.avans.ivh11.Facturering.service.TreatmentService;
import nl.avans.ivh11.Facturering.domain.treatment.Memento.TreatmentCaretaker;
import nl.avans.ivh11.Facturering.domain.treatment.Memento.TreatmentOriginator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@RequestMapping(value = "/treatment")
public class TreatmentController {

    private final Logger logger = LoggerFactory.getLogger(TreatmentController.class);
    private ArrayList<Treatment> treatment = new ArrayList<>();

    //memento
    TreatmentCaretaker caretaker;
    TreatmentOriginator Originator = new TreatmentOriginator();
    @Autowired
    TreatmentMementoService treatmentMementoService;
    @Autowired
    TreatmentCaretakerService treatmentCaretakerService;
    int currentTreatment, savefiles;



    // Views constants
    private final String VIEW_LIST_TREATMENT = "views/treatment/list";
    private final String VIEW_CREATE_TREATMENT = "views/treatment/create";
    private final String VIEW_READ_TREATMENT = "views/treatment/read";

    @Autowired
    private final TreatmentService service;


    // Constructor with Dependency Injection
    public TreatmentController(TreatmentService service, TreatmentMementoService treatmentMementoService,TreatmentCaretakerService treatmentCaretakerService) {
        this.service = service;
        this.treatmentMementoService = treatmentMementoService;
        this.treatmentCaretakerService = treatmentCaretakerService;
        this.caretaker = treatmentCaretakerService.createCareTaker(new TreatmentCaretaker());
    }

    @GetMapping
    public String listTreatments(
            @RequestParam(value="category", required=false, defaultValue="all") String category,
            @RequestParam(value="size", required=false, defaultValue="10") String size,
            Model model) {

        logger.debug("listTreatments called.");
        Iterable<Treatment> treatments = service.getTreatments();

        model.addAttribute("category", category);
        model.addAttribute("size", size);
        model.addAttribute("treatments", treatments);
        return VIEW_LIST_TREATMENT;
    }

    @GetMapping("{id}")
    public ModelAndView view(@PathVariable("id") Treatment treatment, Model model, @PathVariable("id") int i ) {
        savefiles = caretaker.getSize();

       // currentTreatment =caretaker.getSize();
        caretaker.GenerateNewList(i);
        System.out.print(caretaker.getSize());
        model.addAttribute("canRollback", currentTreatment >= 1 && caretaker.getSize() != 0);
        model.addAttribute("canUndo", (caretaker.getSize()-1) > currentTreatment && caretaker.getSize() != 0);

        return new ModelAndView(VIEW_READ_TREATMENT, "treatment", treatment);
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    public String showCreateTreatmentForm(final Treatment treatment, final ModelMap model) {
        logger.debug("showCreateTreatmentForm");
        return VIEW_CREATE_TREATMENT;
    }
    @RequestMapping(value="/new", method = RequestMethod.POST)
    public ModelAndView validateAndSaveTreatment(
            @Valid Treatment treatment,
            final BindingResult bindingResult,
            RedirectAttributes redirect) {

        logger.debug("validateAndSaveTreatment - adding treatment " + treatment.getName());
        if (bindingResult.hasErrors()) {
            logger.debug("validateAndSaveTreatment - not added, bindingResult.hasErrors");
            return new ModelAndView(VIEW_CREATE_TREATMENT, "formErrors", bindingResult.getAllErrors());
        }
        treatment = service.createTreatment(treatment);
        //creating and storing a memento
        Originator.set(treatment);
        caretaker.addmemento(treatmentMementoService.createTrementMemento(Originator.storeInMemento()));
        treatmentCaretakerService.createCareTaker(caretaker);
 //      currentTreatment++;
//       savefiles++;


        //end of the memento
        redirect.addFlashAttribute("globalMessage", "Successfully created a new message");
        return new ModelAndView("redirect:/treatment/{treatment.id}", "treatment.id", treatment.getId());
    }
    @GetMapping(value = "{id}/edit")
    public ModelAndView modifyForm(@PathVariable("id") Treatment treatment) {
        return new ModelAndView(VIEW_CREATE_TREATMENT, "treatment", treatment);
    }

    @GetMapping(value = "{id}/rollback")
    public ModelAndView rollback(@PathVariable("id") int id){
        //caretaker.setCurrentArticle(false);
            currentTreatment--;
            Treatment treatment = Originator.restoreFromMomento(caretaker.getMemento(currentTreatment));
            treatment = service.createTreatment(treatment);

   return new ModelAndView("redirect:/treatment/{treatment.id}", "treatment.id", treatment.getId());
    }

    @GetMapping(value = "{id}/redo")
    public ModelAndView redo(){
        currentTreatment++;
      //  caretaker.setCurrentArticle(true);
        Treatment treatment = Originator.restoreFromMomento(caretaker.getMemento(currentTreatment));
        treatment = service.createTreatment(treatment);
        return new ModelAndView("redirect:/treatment/{treatment.id}", "treatment.id", treatment.getId());
    }
}
