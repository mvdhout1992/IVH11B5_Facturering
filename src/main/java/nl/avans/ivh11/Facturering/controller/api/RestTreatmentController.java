package nl.avans.ivh11.Facturering.controller.api;

import nl.avans.ivh11.Facturering.service.TreatmentService;
import nl.avans.ivh11.Facturering.service.InterpreterService;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.domain.interpreter.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;


import java.util.List;

@RestController
public class RestTreatmentController {
    public static final Logger logger = LoggerFactory.getLogger(RestTreatmentController.class);

    @Autowired
    private final TreatmentService service;
    @Autowired
    private final InterpreterService interpreterService;

    // Constructor with Dependency Injection
    public RestTreatmentController(TreatmentService service, InterpreterService interpreterService) {
        this.service = service;
        this.interpreterService = interpreterService;
    }


    @RequestMapping("/api/treatments/search")
    public List<Treatment> searchTreatments(@RequestParam("query") String query) {
        System.out.println("\nthis is the query: "+query);
        Expression expr = interpreterService.parse(query);
        interpreterService.debugPrintExpressionTree(expr, 0);
        return expr.interpret(service.getTreatments());
        //return service.getTreatments();
    }

    @RequestMapping(value = "/api/treatments/", method = RequestMethod.GET)
    public ResponseEntity<List<Treatment>> listAllUsers() {
        List<Treatment> treatments = service.getTreatments();
        if (treatments.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
            // You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Treatment>>(treatments, HttpStatus.OK);
    }


    @RequestMapping(value = "/api/treatments/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTreatment(@PathVariable("id") Long id) {
        logger.info("Fetching User with id {}", id);
        Treatment treatment = service.getTreatmentById(id);
        if (treatment == null) {
            logger.error("User with id {} not found.", id);
            return new ResponseEntity( HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Treatment>(treatment, HttpStatus.OK);
    }


    @RequestMapping(value = "/api/treatments/", method = RequestMethod.POST)
    public ResponseEntity<?> createTreatment(@RequestBody Treatment treatment, UriComponentsBuilder ucBuilder) {
        logger.info("Creating User : {}", treatment);

        service.createTreatment(treatment);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/createtreatment/{id}").buildAndExpand(treatment.getId()).toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/api/treatments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        logger.info("Fetching & Deleting User with id {}", id);

        Treatment treatment = service.getTreatmentById(id);
        if (treatment == null) {
            logger.error("Unable to delete. User with id {} not found.", id);
        }
        service.removeTreatmentById(id);
        logger.trace("removed");
        return new ResponseEntity<Treatment>(HttpStatus.NO_CONTENT);
    }


    @RequestMapping(value = "/api/treatments/", method = RequestMethod.DELETE)
    public ResponseEntity<Treatment> deleteAllTreatments() {
        logger.info("Deleting All Treatments");

        service.deleteAllTreatments();
        return new ResponseEntity<Treatment>(HttpStatus.NO_CONTENT);
    }


    @RequestMapping(value = "/api/treatments/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTreatment(@PathVariable("id") Long id, @RequestBody Treatment treatment) {
        logger.info("Updating treatment with id {}", id);

        Treatment currentTreatment = service.getTreatmentById(id);

        if (currentTreatment == null) {
            logger.error("Unable to update. treatment with id {} not found.", id);
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        currentTreatment.setName(treatment.getName());
        currentTreatment.setCode(treatment.getCode());

        service.updateTreatment(currentTreatment);
        return new ResponseEntity<Treatment>(currentTreatment, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/treatments/", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTreatmentAll( @RequestBody Treatment treatment) {
        logger.info("Updating treatment with id {}");
        List<Treatment> treatments = service.getTreatments();

        for(Treatment a : treatments) {
            Treatment currentTreatment = service.getTreatmentById(a.getId());

            if (currentTreatment == null) {
                logger.error("Unable to update. treatment with id {} not found.", a.getId());

            }
            currentTreatment.setName(treatment.getName());
            currentTreatment.setCode(treatment.getCode());

            service.updateTreatment(currentTreatment);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}
