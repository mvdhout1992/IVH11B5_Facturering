package nl.avans.ivh11.Facturering.controller;

import nl.avans.ivh11.Facturering.domain.customer.Customer;
import nl.avans.ivh11.Facturering.domain.insurance.Insurance;
import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.DiscountStrategy;
import nl.avans.ivh11.Facturering.service.CustomerService;
import nl.avans.ivh11.Facturering.service.InsuranceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/customer")
public class CustomerController {

    private final Logger logger = LoggerFactory.getLogger(CustomerController.class);
    private ArrayList<Customer> customers = new ArrayList<>();

    // Views constants
    private final String VIEW_LIST_CUSTOMER = "views/customer/list";
    private final String VIEW_CREATE_CUSTOMER = "views/customer/create";
    private final String VIEW_READ_CUSTOMER = "views/customer/read";
    private final String VIEW_CUSTOMER_ADD_INSURANCE = "views/customer/addInsurance";

    @Autowired
    private final CustomerService service;

    @Autowired
    private final InsuranceService insuranceService;

    // Constructor with Dependency Injection
    public CustomerController(CustomerService service, InsuranceService insuranceService) {
        this.service = service;
        this.insuranceService = insuranceService;
    }

    @GetMapping
    public String listCustomers(
            @RequestParam(value="category", required=false, defaultValue="all") String category,
            @RequestParam(value="size", required=false, defaultValue="10") String size,
            Model model) {

        logger.debug("listCustomers called.");
            Iterable<Customer> customers = service.getCustomers();

        model.addAttribute("category", category);
        model.addAttribute("size", size);
        model.addAttribute("customers", customers);
        return VIEW_LIST_CUSTOMER;
    }

    @GetMapping("{id}")
    public ModelAndView view(@PathVariable("id") Customer customer) {
        return new ModelAndView(VIEW_READ_CUSTOMER, "customer", customer);
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    public String showCreateCustomerForm(final Customer customer, final ModelMap model) {
        logger.debug("showCreateCustomerForm");
        return VIEW_CREATE_CUSTOMER;
    }

    @RequestMapping(value="/new", method = RequestMethod.POST)
    public ModelAndView validateAndSaveCustomer(
            @Valid Customer customer,
            final BindingResult bindingResult,
            RedirectAttributes redirect) {

        logger.debug("validateAndSaveCustomer - adding customer " + customer.getName());
        if (bindingResult.hasErrors()) {
            logger.debug("validateAndSaveCustomer - not added, bindingResult.hasErrors");
            return new ModelAndView(VIEW_CREATE_CUSTOMER, "formErrors", bindingResult.getAllErrors());
        }

        customer = service.createCustomer(customer);

        redirect.addFlashAttribute("globalMessage", "Successfully created a new message");
        return new ModelAndView("redirect:/customer/{customer.id}", "customer.id", customer.getId());
    }

    /**
     * Gets a list of all the <code>Insurances</code>.
     *
     * @return A list of <code>Insurances</code>
     */
    @ModelAttribute("insurances")
    public List<Insurance> getInsurances() {
        List<Insurance> list = insuranceService.getBaseInsurances();
        return list;
    }

    @ModelAttribute("discounts")
    public List<DiscountStrategy> getDiscounts(){
        List<DiscountStrategy> list = insuranceService.getDiscounts();
        return list;
    }

    @GetMapping(value = "{id}/edit")
    public ModelAndView modifyForm(@PathVariable("id") Customer customer) {
        return new ModelAndView(VIEW_CREATE_CUSTOMER, "customer", customer);
    }
}
