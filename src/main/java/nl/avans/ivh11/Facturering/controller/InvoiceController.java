package nl.avans.ivh11.Facturering.controller;

import nl.avans.ivh11.Facturering.InvoiceGenerator.PDFBuilder.InvoicePDFDirector;
import nl.avans.ivh11.Facturering.InvoiceGenerator.PDFBuilder.PDFBuilder;
import nl.avans.ivh11.Facturering.domain.customer.Customer;
import nl.avans.ivh11.Facturering.domain.invoice.Invoice;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.service.CustomerService;
import nl.avans.ivh11.Facturering.service.InvoiceService;
import nl.avans.ivh11.Facturering.service.TreatmentService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import nl.avans.ivh11.Facturering.domain.invoice.state.InvoiceCreatedState;
import nl.avans.ivh11.Facturering.domain.invoice.state.InvoiceGeneratedState;
import nl.avans.ivh11.Facturering.domain.invoice.state.InvoicePaidState;

@Controller
@RequestMapping(value = "/invoice")
public class InvoiceController {

    private final Logger logger = LoggerFactory.getLogger(InvoiceController.class);
    private ArrayList<Invoice> invoices = new ArrayList<>();
    private InvoiceCreatedState invoiceCreatedState = new InvoiceCreatedState();
    private InvoiceGeneratedState invoiceGeneratedState = new InvoiceGeneratedState();
    private InvoicePaidState invoicePaidState = new InvoicePaidState();

    // Views constants
    private final String VIEW_LIST_INVOICE = "views/invoice/list";
    private final String VIEW_CREATE_INVOICE = "views/invoice/create";
    private final String VIEW_READ_INVOICE = "views/invoice/read";

    @Autowired
    private final InvoiceService service;
    @Autowired
    private final CustomerService customerService;
    @Autowired
    private final TreatmentService treatmentService;

    // Constructor with Dependency Injection
    public InvoiceController(InvoiceService service, CustomerService customerService, TreatmentService treatmentService)
    {
        this.service = service;
        this.customerService = customerService;
        this.treatmentService = treatmentService;
    }

    @GetMapping
    public String listInvoices(
            @RequestParam(value="category", required=false, defaultValue="all") String category,
            @RequestParam(value="size", required=false, defaultValue="10") String size,
            Model model) {

        logger.debug("listInvoices called.");
        Iterable<Invoice> invoices = service.getInvoices();

        model.addAttribute("category", category);
        model.addAttribute("size", size);
        model.addAttribute("invoices", invoices);
        return VIEW_LIST_INVOICE;
    }

    @GetMapping("{id}")
    public ModelAndView view(@PathVariable("id") Invoice invoice) {
        return new ModelAndView(VIEW_READ_INVOICE, "invoice", invoice);
    }

    @RequestMapping(value = "{id}/updatepaymentstatus", method = RequestMethod.GET)
    public ModelAndView updatePaymentStatus(@PathVariable("id") int id) {
        
        Invoice invoice = service.getInvoice(Long.valueOf(id));
        invoice.setHasPaid(true);
        service.createOrUpdateInvoice(invoice);
        
        invoicePaidState.doAction(invoice);
        
        return new ModelAndView("redirect:/invoice");
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    public String showCreateInvoiceForm(final Invoice invoice, final ModelMap model) {
        logger.debug("showCreateInvoiceForm");
        return VIEW_CREATE_INVOICE;
    }

    @Transactional
    @RequestMapping(value="/new", method = RequestMethod.POST)
    public ModelAndView validateAndSaveInvoice(
            @Valid Invoice invoice,
            final BindingResult bindingResult,
            RedirectAttributes redirect) {

        logger.debug("validateAndSaveInvoice - adding Invoice " + invoice.getName());
        if (bindingResult.hasErrors()) {
            logger.debug("validateAndSaveInvoice - not added, bindingResult.hasErrors");
            return new ModelAndView(VIEW_CREATE_INVOICE, "formErrors", bindingResult.getAllErrors());
        }

        ArrayList<Treatment> treatments = new ArrayList<>();
        for(Treatment t :invoice.getTreatments()) {
            Treatment t2 = treatmentService.getTreatmentByName(t.getName());
            treatments.add(t2);
        }
        invoice.setTreatments(treatments);

        invoice = service.createOrUpdateInvoice(invoice);
        invoiceCreatedState.doAction(invoice);

        redirect.addFlashAttribute("globalMessage", "Successfully created a new message");
        return new ModelAndView("redirect:/invoice/{invoice.id}", "invoice.id", invoice.getId());
    }

    @GetMapping(value = "{id}/edit")
    public ModelAndView modifyForm(@PathVariable("id") Invoice invoice, Model model) {
        model.addAttribute("customers", customerService.getCustomers());
        return new ModelAndView(VIEW_CREATE_INVOICE, "invoice", invoice);
    }

    @RequestMapping(value = "/generate/pdf/{id}", method = RequestMethod.GET)
    public String generatePDFForOneInvoice(HttpServletResponse response, @PathVariable("id") Invoice invoice) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            (new InvoicePDFDirector(new PDFBuilder())).Construct().generatePDF(invoice, out);
        } catch(IOException ex) {
            logger.error("Unable to generate PDF", ex);
        }

        try {
            byte[] documentInBytes = out.toByteArray();
            response.setDateHeader("Expires", -1);
            response.setContentType("application/pdf");
            response.setContentLength(documentInBytes.length);
            response.getOutputStream().write(documentInBytes);
        } catch (Exception ioe) {
            logger.error("Unable to write to document", ioe);
        }
        
        service.addObservers(invoice);
        invoiceGeneratedState.doAction(invoice);

        return null;
    }


    @RequestMapping(value="/new", params={"addRow"})
    public String changeAddRow(final Invoice invoice) {
        invoice.addTreatment(new Treatment());
        return VIEW_CREATE_INVOICE;
    }

    @RequestMapping(value="/new", params={"removeRow"})
    public String changeRemoveRow(final Invoice invoice, final HttpServletRequest req) {
        final Integer id = Integer.valueOf(req.getParameter("removeRow"));
        List<Treatment> tList = invoice.getTreatments();
        tList.remove(id);
        invoice.setTreatments(tList);
        return VIEW_CREATE_INVOICE;
    }


    @ModelAttribute("loadedTreatments")
    public List<Treatment> getTreatments() {
        return treatmentService.getTreatments();
    }

    @ModelAttribute("customers")
    public List<Customer> getCustomers() {
        return customerService.getCustomers();
    }
}
