package nl.avans.ivh11.Facturering.controller;

import nl.avans.ivh11.Facturering.domain.insurance.Insurance;
import nl.avans.ivh11.Facturering.domain.insurancecompany.InsuranceCompany;
import nl.avans.ivh11.Facturering.domain.invoice.Invoice;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.service.InsuranceCompanyService;
import nl.avans.ivh11.Facturering.service.InsuranceService;
import nl.avans.ivh11.Facturering.service.TreatmentService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "/insurance")
public class InsuranceController {

    private final Logger logger = LoggerFactory.getLogger(InsuranceController.class);
    private ArrayList<Insurance> insurances = new ArrayList<>();

    // Views constants
    private final String VIEW_LIST_INSURANCE = "views/insurance/list";
    private final String VIEW_CREATE_INSURANCE = "views/insurance/create";
    private final String VIEW_READ_INSURANCE = "views/insurance/read";

    @Autowired
    private final InsuranceService service;
    @Autowired
    private final InsuranceCompanyService insuranceCompanyService;
    @Autowired
    private final TreatmentService treatmentService;

    // Constructor with Dependency Injection
    public InsuranceController(InsuranceService service, InsuranceCompanyService insuranceCompanyService,
                               TreatmentService treatmentService) {
        this.service = service;
        this.insuranceCompanyService = insuranceCompanyService;
        this.treatmentService = treatmentService;
    }

    @GetMapping
    public String listInsurances(
            @RequestParam(value="category", required=false, defaultValue="all") String category,
            @RequestParam(value="size", required=false, defaultValue="10") String size,
            Model model) {

        logger.debug("listInsurances called.");
        Iterable<Insurance> insurances = service.getBaseInsurances();

        model.addAttribute("category", category);
        model.addAttribute("size", size);
        model.addAttribute("insurances", insurances);
        return VIEW_LIST_INSURANCE;
    }

    @GetMapping("{id}")
    public ModelAndView view(@PathVariable("id") Insurance insurance) {
        return new ModelAndView(VIEW_READ_INSURANCE, "insurance", insurance);
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    public String showCreateInsuranceForm(final Insurance insurance, final ModelMap model) {
        logger.debug("showCreateInsuranceForm");
        return VIEW_CREATE_INSURANCE;
    }

    @RequestMapping(value="/new", method = RequestMethod.POST)
    public ModelAndView validateAndSaveInsurance(
            @Valid Insurance insurance,
            final BindingResult bindingResult,
            RedirectAttributes redirect) {

        logger.debug("validateAndSaveInsurance - adding insurance " + insurance.getName());
        if (bindingResult.hasErrors()) {
            logger.debug("validateAndSaveInsurance - not added, bindingResult.hasErrors");
            return new ModelAndView(VIEW_CREATE_INSURANCE, "formErrors", bindingResult.getAllErrors());
        }

        ArrayList<Treatment> treatments = new ArrayList<>();
        for(Treatment t :insurance.getIncludedTreatments()) {
            Treatment t2 = treatmentService.getTreatmentByName(t.getName());
            treatments.add(t2);
        }

        insurance.setIncludedTreatments(treatments);
        insurance = service.createInsurance(insurance);

        redirect.addFlashAttribute("globalMessage", "Successfully created a new message");
        return new ModelAndView("redirect:/insurance/{insurance.id}", "insurance.id", insurance.getId());
    }

    @GetMapping(value = "{id}/edit")
    public ModelAndView modifyForm(@PathVariable("id") Insurance insurance) {
        return new ModelAndView(VIEW_CREATE_INSURANCE, "insurance", insurance);
    }

    @RequestMapping(value="/new", params={"addRow"})
    public String changeAddRow(final Insurance insurance) {
        ArrayList<Treatment> tList = new ArrayList<Treatment>();
        tList.add(new Treatment());
        insurance.addIncludedTreatments(tList);
        return VIEW_CREATE_INSURANCE;
    }

    @RequestMapping(value="/new", params={"removeRow"})
    public String changeRemoveRow(final Insurance insurance, final HttpServletRequest req) {
        final Integer id = Integer.valueOf(req.getParameter("removeRow"));
        return VIEW_CREATE_INSURANCE;
    }

    @ModelAttribute("loadedTreatments")
    public List<Treatment> getTreatments() {
        return treatmentService.getTreatments();
    }

    @ModelAttribute("insurancecompanies")
    public List<InsuranceCompany> getInsuranceCompanies() {
        return insuranceCompanyService.getInsuranceCompanies();
    }
}
