package nl.avans.ivh11.Facturering.controller;

import nl.avans.ivh11.Facturering.domain.insurancecompany.InsuranceCompany;
import nl.avans.ivh11.Facturering.service.InsuranceCompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
@RequestMapping(value = "/insurance_company")
public class InsuranceCompanyController {

    private final Logger logger = LoggerFactory.getLogger(InsuranceCompanyController.class);
    private ArrayList<InsuranceCompany> insuranceCompanies = new ArrayList<>();

    // Views constants
    private final String VIEW_LIST_INSURANCE_COMPANY = "views/insurance_company/list";
    private final String VIEW_CREATE_INSURANCE_COMPANY = "views/insurance_company/create";
    private final String VIEW_READ_INSURANCE_COMPANY = "views/insurance_company/read";

    @Autowired
    private final InsuranceCompanyService service;

    // Constructor with Dependency Injection
    public InsuranceCompanyController(InsuranceCompanyService service) {
        this.service = service;
    }

    @GetMapping
    public String listInsuranceCompanies(
            @RequestParam(value="category", required=false, defaultValue="all") String category,
            @RequestParam(value="size", required=false, defaultValue="10") String size,
            Model model) {

        logger.debug("listInsuranceCompanies called.");
        Iterable<InsuranceCompany> insuranceCompanies = service.getInsuranceCompanies();

        model.addAttribute("category", category);
        model.addAttribute("size", size);
        model.addAttribute("insuranceCompanies", insuranceCompanies);
        return VIEW_LIST_INSURANCE_COMPANY;
    }

    @GetMapping("{id}")
    public ModelAndView view(@PathVariable("id") InsuranceCompany insuranceCompany) {
        return new ModelAndView(VIEW_READ_INSURANCE_COMPANY, "insuranceCompany", insuranceCompany);
    }

    @RequestMapping(value="/new", method = RequestMethod.GET)
    public String showCreateInsuranceCompanyForm(final InsuranceCompany insuranceCompany, final ModelMap model) {
        logger.debug("showCreateInsuranceCompanyForm");
        return VIEW_CREATE_INSURANCE_COMPANY;
    }

    @RequestMapping(value="/new", method = RequestMethod.POST)
    public ModelAndView validateAndSaveInsuranceCompany(
            @Valid InsuranceCompany insuranceCompany,
            final BindingResult bindingResult,
            RedirectAttributes redirect) {

        logger.debug("validateAndSaveInsuranceCompany - adding insuranceCompany " + insuranceCompany.getName());
        if (bindingResult.hasErrors()) {
            logger.debug("validateAndSaveInsuranceCompany - not added, bindingResult.hasErrors");
            return new ModelAndView(VIEW_CREATE_INSURANCE_COMPANY, "formErrors", bindingResult.getAllErrors());
        }

        insuranceCompany = service.createInsuranceCompany(insuranceCompany);

        redirect.addFlashAttribute("globalMessage", "Successfully created a new message");
        return new ModelAndView("redirect:/insurance_company/{insuranceCompany.id}",
                "insuranceCompany.id", insuranceCompany.getId());
    }

    @GetMapping(value = "{id}/edit")
    public ModelAndView modifyForm(@PathVariable("id") InsuranceCompany insuranceCompany) {
        return new ModelAndView(VIEW_CREATE_INSURANCE_COMPANY, "insuranceCompany", insuranceCompany);
    }

}
