package nl.avans.ivh11.Facturering.InvoiceGenerator;

import nl.avans.ivh11.Facturering.InvoiceGenerator.PDFComposite.*;
import nl.avans.ivh11.Facturering.domain.customer.Customer;
import nl.avans.ivh11.Facturering.domain.insurancecompany.InsuranceCompany;
import nl.avans.ivh11.Facturering.domain.invoice.Invoice;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import org.apache.log4j.Logger;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.encoding.Encoding;
import org.apache.pdfbox.encoding.EncodingManager;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 *
 * @author maikel
 */
public class InvoicePDFGenerator extends PDFGenerator {
    private static final Logger LOGGER = Logger.getLogger(InvoicePDFGenerator.class);

    private int maxSessionsPerPage = 23;
    private String lineBreak = "_________________________________________";
    private int smallTextSize = 10;
    private int defaultTextSize = 12;
    private int largeTextSize = 20;
    private int middleHeightOffset = 427;
    private int invoiceWidth = 40;
    private int customerWidth = 60;
    private int insuranceCompanyWidth = 380;
    private int titleHeight = 440;
    private int lengthX = 47;
    private int descriptionX = 130;
    private int amountX = 340;
    private int totalX = 420;
    private int vatX= 500;
    private int invoiceLineBreakHeight= 434;
    private int bottomTextOffsetHeight2= 27;
    private int bottomTextOffsetHeight1= 40;
    private int middleHeightTotalValueAdjust = 65;
    private int middleHeightTotalAdjust = 65;
    private int middleHeightEndLineBreakOffset = 272;
    private int middleHeightVatValueAdjust= 43;
    private int middleHeightVatAdjust= 43;
    private int middleHeightSubtotalValueAdjust = 26;
    private int middleHeightSubtotalAdjust = 26;
    private int middleHeightOffsetLineBreakAdjust = 5;
    private int middleHeightOffsetAdjust = 13;
    private int middleHeightLineBreakAdjust= 47;
    private int invoiceExpirationDateHeight = 517;
    private int invoiceDateHeight = 530;
    private int invoiceNameHeight = 520;
    private int customerZipCodeHeight = 589;
    private int customerStreetHeight = 602;
    private int customerFullNameHeight = 615;
    private int insuranceCompanyBankHeight = 674;
    private int insuranceCompanyTaxNumberHeight = 687;
    private int insuranceCompanyKvKHeight = 700;
    private int insuranceCompanyAddressHeight = 724;
    private int insuranceCompanyStreetHeight = 737;
    private int insuranceCompanyNameHeight = 750;
    private int invoiceEndLineBreakAdjust = 60;
    private int customerDeductablesHeight = 504;
  

    public InvoicePDFGenerator(int maxSessionsPerPage, int smallTextSize, int defaultTextSize, int largeTextSize) throws IOException {
      super();
      this.maxSessionsPerPage = maxSessionsPerPage;
      this.smallTextSize = smallTextSize;
      this.defaultTextSize = defaultTextSize;
      this.largeTextSize = largeTextSize;
    }
  
    public void generatePDF(Invoice invoice, OutputStream out){
         
        int sessionsRemaining = invoice.getSessionsCount();
        int sessionIndex = 0;     

        try {
            while (sessionsRemaining - maxSessionsPerPage > 0) {
                PDPage additionalPage = new PDPage();
                getDocument().addPage(additionalPage);
                generatePDFPage(invoice, sessionIndex, false, getDocument(), additionalPage);
                sessionIndex += maxSessionsPerPage;
                sessionsRemaining -= maxSessionsPerPage;
            }

            PDPage page = new PDPage();
            getDocument().addPage(page);
            generatePDFPage(invoice, sessionIndex, true, getDocument(), page);
            saveAndClose(out);
        } catch (IOException ex){
            LOGGER.error("Unable to generate pdf", ex);
        }
   }

    private void generatePDFPage(Invoice invoice, int sessionIndex,
                                  boolean lastPage, PDDocument document, PDPage page) throws IOException {

        // Create a new font object selecting one of the PDF base fonts
        PDFont font = PDType1Font.HELVETICA_BOLD;

        // Font sizes
        int defaultSize = this.defaultTextSize;
        int smallSize = this.smallTextSize;
        int largeSize = this.largeTextSize;

        // Height offset for dynamic listing of Treatment and costs
        int middleHeightOffset = this.middleHeightOffset;

        Encoding e = EncodingManager.INSTANCE.getEncoding(COSName.WIN_ANSI_ENCODING);
        String euroSign = String.valueOf(Character.toChars(e.getCode("Euro")));

        PDPageContentStream contentStream = new PDPageContentStream(document, page);

        // width, height
        InsuranceCompany ic = invoice.getCustomer().getInsurance().getInsuranceCompany();

        PDFPage pdfPage = new PDFPage(contentStream,500);

        PDFSection insuranceCompanySection = getPdfInsuranceCompanyInfoSection(font, defaultSize, ic);

        Customer c = invoice.getCustomer();
        PDFSection customerSection = getPdfCustomerInfoSection(font, defaultSize, c);

        PDFSection invoiceTitleSection = getPdfInvoiceTitleSection(invoice, font, defaultSize);

        String dateStr = new SimpleDateFormat("dd/MM/yyyy").format(invoice.getDate());
        String expDateStr = new SimpleDateFormat("dd/MM/yyyy").format(invoice.getExpirationDate());

        PDFSection invoiceSummarySection = getPDFInvoiceSummarySection(font, defaultSize, c, dateStr, expDateStr);

        PDFSection invoiceSummaryHeaderSection = getPdfInvoiceSummaryHeaderSection(font, defaultSize);

        int sessionsCount = invoice.getSessionsCount();
        for (int i = sessionIndex; i < sessionIndex + this.maxSessionsPerPage && i < sessionsCount; i++) {
            middleHeightOffset -= this.middleHeightOffsetAdjust;
            Treatment t = invoice.getTreatments().get(i);

            PDFSection invoiceTreatmentSection = getPdfInvoiceTreatmentSection(font, defaultSize, middleHeightOffset, euroSign, t);

            pdfPage.addChild(invoiceTreatmentSection);
        }

        PDFSection invoiceTreatmentsEndSection = getPdfInvoiceTreatmentsEndSection(font, defaultSize, middleHeightOffset);


        if (lastPage) {
            // VAT is BTW in Dutch
            BigDecimal valueAddedTaxes = new BigDecimal("0.19");
            String totaalStr = this.getCurrenyStringFromBigDecimal(invoice.calculateTotal(valueAddedTaxes));

            PDFSection invoiceTreatmentsCostSection = getPdfInvoiceTreatmentsCostSection(invoice, font, defaultSize, middleHeightOffset, euroSign, valueAddedTaxes, totaalStr);

            pdfPage.addChild(invoiceTreatmentsCostSection);

            PDFSection bottomTextSection = getPdfBottomTextSection(invoice, font, defaultSize, smallSize, euroSign, expDateStr, totaalStr);

            pdfPage.addChild(bottomTextSection);
        }


        pdfPage.addChild(insuranceCompanySection);
        pdfPage.addChild(customerSection);
        pdfPage.addChild(insuranceCompanySection);
        pdfPage.addChild(invoiceSummarySection);
        pdfPage.addChild(invoiceTitleSection);
        pdfPage.addChild(invoiceSummaryHeaderSection);
        pdfPage.addChild(invoiceTreatmentsEndSection);

        pdfPage.draw();

        contentStream.close();
    }

    private PDFSection getPdfInvoiceTreatmentsCostSection(Invoice invoice, PDFont font, int defaultSize, int middleHeightOffset, String euroSign, BigDecimal valueAddedTaxes, String totaalStr) {
        PDFSection invoiceTreatmentsCostSection = new PDFSection(this.amountX, middleHeightOffset-this.middleHeightSubtotalAdjust, 20);
        PDFRow subTotalRow = new PDFRow(100);

        PDFText text7_1 = new PDFText(font, defaultSize, "Subtotaal");
        subTotalRow.addChild(text7_1);
        PDFText text7_2 = new PDFText(font, defaultSize, euroSign +
                this.getCurrenyStringFromBigDecimal(invoice.calculateSubtotal()));
        subTotalRow.addChild(text7_2);

        PDFRow VATTotalRow = new PDFRow(100);

        PDFText text8_1 = new PDFText(font, defaultSize, "19% BTW");
        VATTotalRow.addChild(text8_1);
        PDFText text8_2 = new PDFText(font, defaultSize, euroSign +
                this.getCurrenyStringFromBigDecimal(invoice.calculateVAT(valueAddedTaxes)));
        VATTotalRow.addChild(text8_2);

        PDFRow totalRow = new PDFRow(100);
        PDFText text9_1 = new PDFText(font, defaultSize, "Totaal:");
        totalRow.addChild(text9_1);
        PDFText text9_2 = new PDFText(font, defaultSize, euroSign + totaalStr);
        totalRow.addChild(text9_2);

        invoiceTreatmentsCostSection.addChild(subTotalRow);
        invoiceTreatmentsCostSection.addChild(VATTotalRow);
        invoiceTreatmentsCostSection.addChild(new PDFText(font, defaultSize, "______________________________"));
        invoiceTreatmentsCostSection.addChild(totalRow);
        return invoiceTreatmentsCostSection;
    }

    private PDFSection getPdfBottomTextSection(Invoice invoice, PDFont font, int defaultSize, int smallSize, String euroSign, String expDateStr, String totaalStr) {
        PDFSection bottomTextSection = new PDFSection(this.invoiceWidth, invoiceEndLineBreakAdjust, 20);

        PDFRow bottomTextLinebreakRow = new PDFRow(250);
        PDFLineBreak bottomTextLinebreak1 = new PDFLineBreak(font, defaultSize);
        bottomTextLinebreakRow.addChild(bottomTextLinebreak1);
        PDFLineBreak bottomTextLinebreak2 = new PDFLineBreak(font, defaultSize);
        bottomTextLinebreakRow.addChild(bottomTextLinebreak2);

        bottomTextSection.addChild(bottomTextLinebreakRow);

        PDFText text10_1 = new PDFText(font, smallSize, "We verzoeken u vriendelijk het bovenstaande bedrag "
                + "van " + euroSign + totaalStr +" voor " +  expDateStr + " te voldoen op onze");
        bottomTextSection.addChild(text10_1);
        PDFText textt10_2 = new PDFText(font, smallSize, "bankrekening onder vermelding van het factuurnummer " +
                invoice.getId() +". Voor vragen kunt u contact opnemen per e-mail.");
        bottomTextSection.addChild(textt10_2);
        return bottomTextSection;
    }

    private PDFSection getPdfInvoiceTreatmentsEndSection(PDFont font, int defaultSize, int middleHeightOffset) {
        PDFSection invoiceTreatmentsEndSection = new PDFSection(this.lengthX,
                middleHeightOffset-this.middleHeightOffsetLineBreakAdjust, 20);


        PDFRow treatmentEndLineBreakRow = new PDFRow(250);
        invoiceTreatmentsEndSection.addChild(treatmentEndLineBreakRow);

        PDFLineBreak treatmentEndHeaderLineBreak = new PDFLineBreak(font, defaultSize);
        treatmentEndLineBreakRow.addChild(treatmentEndHeaderLineBreak);
        PDFLineBreak treatmentEndHeaderLineBreak2 = new PDFLineBreak(font, defaultSize);
        treatmentEndLineBreakRow.addChild(treatmentEndHeaderLineBreak2);
        return invoiceTreatmentsEndSection;
    }

    private PDFSection getPdfInvoiceTreatmentSection(PDFont font, int defaultSize, int middleHeightOffset, String euroSign, Treatment t) {
        PDFSection invoiceTreatmentSection = new PDFSection(this.lengthX, middleHeightOffset, 20);
        PDFRow treatmentRow = new PDFRow(100);
        invoiceTreatmentSection.addChild(treatmentRow);

        PDFText text6_1 = new PDFText(font, defaultSize, t.getTotalSessionLength() + " uur");
        treatmentRow.addChild(text6_1);
        PDFText text6_2 = new PDFText(font, defaultSize, t.getName());
        treatmentRow.addChild(text6_2);
        PDFText text6_3 = new PDFText(font, defaultSize, euroSign +
                this.getCurrenyStringFromBigDecimal(t.getPriceRate()));
        treatmentRow.addChild(text6_3);
        BigDecimal subTotal = t.getSubTotal();
        PDFText text6_4 = new PDFText(font, defaultSize, euroSign +
                this.getCurrenyStringFromBigDecimal(subTotal));
        treatmentRow.addChild(text6_4);
        PDFText text6_5 = new PDFText(font, defaultSize, "19%");
        treatmentRow.addChild(text6_5);
        return invoiceTreatmentSection;
    }

    private PDFSection getPdfInvoiceSummaryHeaderSection(PDFont font, int defaultSize) {
        PDFSection invoiceSummaryHeaderSection = new PDFSection(this.lengthX, this.titleHeight, 10);
        PDFRow headerRow = new PDFRow(100);
        invoiceSummaryHeaderSection.addChild(headerRow);

        PDFText text5_1 = new PDFText(font, defaultSize, "Lengte");
        headerRow.addChild(text5_1);
        PDFText text5_2 = new PDFText(font, defaultSize, "Omschrijving");
        headerRow.addChild(text5_2);
        PDFText text5_3 = new PDFText(font, defaultSize, "Bedrag");
        headerRow.addChild(text5_3);
        PDFText text5_4 = new PDFText(font, defaultSize, "Totaal");
        headerRow.addChild(text5_4);
        PDFText text5_5 = new PDFText(font, defaultSize, "BTW");
        headerRow.addChild(text5_5);

        PDFRow lineBreakRow = new PDFRow(250);
        invoiceSummaryHeaderSection.addChild(lineBreakRow);

        PDFLineBreak headerLineBreak = new PDFLineBreak(font, defaultSize);
        lineBreakRow.addChild(headerLineBreak);
        PDFLineBreak headerLineBreak2 = new PDFLineBreak(font, defaultSize);
        lineBreakRow.addChild(headerLineBreak2);
        return invoiceSummaryHeaderSection;
    }

    private PDFSection getPDFInvoiceSummarySection(PDFont font, int defaultSize, Customer c, String dateStr, String expDateStr) {
        PDFSection invoiceSummarySection = new PDFSection(this.insuranceCompanyWidth, this.invoiceDateHeight, 20);
        PDFText text4_1 = new PDFText(font, defaultSize, "Factuurdatum:          " + dateStr);
        invoiceSummarySection.addChild(text4_1);
        PDFText text4_2 = new PDFText(font, defaultSize, "Vervaldatum:            " + expDateStr);
        invoiceSummarySection.addChild(text4_2);
        PDFText text4_3 = new PDFText(font, defaultSize, "Huidig Eigen Risico:            " + c.getDeductables());
        invoiceSummarySection.addChild(text4_3);
        return invoiceSummarySection;
    }

    private PDFSection getPdfInvoiceTitleSection(Invoice invoice, PDFont font, int defaultSize) {
        PDFSection invoiceTitleSection = new PDFSection(this.invoiceWidth, this.invoiceNameHeight, 20);
        PDFText text3_1 = new PDFText(font, defaultSize, "Factuur " + invoice.getName());
        invoiceTitleSection.addChild(text3_1);
        return invoiceTitleSection;
    }

    private PDFSection getPdfCustomerInfoSection(PDFont font, int defaultSize, Customer c) {
        PDFSection customerSection = new PDFSection(this.customerWidth, this.customerFullNameHeight, 20);

        PDFText text2_1 = new PDFText(font, defaultSize, c.getName().toString());
        customerSection.addChild(text2_1);
        PDFText text2_2 = new PDFText(font, defaultSize, c.getAddress().getStreet());
        customerSection.addChild(text2_2);
        PDFText text2_3 = new PDFText(font, defaultSize, c.getAddress().getZipCode());
        customerSection.addChild(text2_3);
        return customerSection;
    }

    private PDFSection getPdfInsuranceCompanyInfoSection(PDFont font, int defaultSize, InsuranceCompany ic) {
        PDFSection insuranceCompanySection = new PDFSection(this.insuranceCompanyWidth, insuranceCompanyNameHeight, 20);
        PDFText text1 = new PDFText(font, defaultSize, ic.getName());
        insuranceCompanySection.addChild(text1);
        PDFText text2 = new PDFText(font, defaultSize, ic.getAddress().getStreet());
        insuranceCompanySection.addChild(text2);
        PDFText text3 = new PDFText(font, defaultSize, ic.getAddress().getZipCode());
        insuranceCompanySection.addChild(text3);
        PDFText text4 = new PDFText(font, defaultSize, ic.getKvK());
        insuranceCompanySection.addChild(text4);
        PDFText text5 = new PDFText(font, defaultSize, ic.getTaxNumber());
        insuranceCompanySection.addChild(text5);
        PDFText text6 = new PDFText(font, defaultSize, ic.getBank());
        insuranceCompanySection.addChild(text6);
        
        return insuranceCompanySection;
    }


    private String getCurrenyStringFromBigDecimal(BigDecimal bd) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.GERMAN);
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat("#,###.00", otherSymbols);
        return df.format(bd);
    }
}