package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFComposite;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import java.io.IOException;

public class PDFPage extends PDFComponent {
    PDPageContentStream contentStream;
    int ySpacingPerSection;

    public PDFPage(PDPageContentStream contentStream, int ySpacingPerSection) {
        this.contentStream = contentStream;
        this.ySpacingPerSection = ySpacingPerSection;
    }

    public void draw(PDPageContentStream contentStream, int _x, int _y) throws IOException{
        int x = _x;
        int y = _y;

        for (PDFComponent child : children) {

            child.draw(contentStream, x, y);
            y += ySpacingPerSection;
        }
    }

    public void draw() throws IOException{
        draw(contentStream, 0 , 0);
    }

    public void addChild(PDFComponent child) {
        this.children.add(child);
    }
}
