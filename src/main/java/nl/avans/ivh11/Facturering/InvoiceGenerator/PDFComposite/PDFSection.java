package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFComposite;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import java.io.IOException;

public class PDFSection extends PDFComponent {
    int ySpacingPerChild;

    public PDFSection(int x, int y, int ySpacingPerChild) {
        super(x,  y);
        this.ySpacingPerChild = ySpacingPerChild;
    }

    public void draw(PDPageContentStream contentStream, int _x, int _y) throws IOException{

        int x = this.x;
        int y = this.y;
        for (PDFComponent child : children) {

            child.draw(contentStream, x, y);
            y -= ySpacingPerChild;
        }
    }

    public void addChild(PDFComponent child) {
        this.children.add(child);
    }

}
