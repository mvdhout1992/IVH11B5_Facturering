package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFComposite;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

import java.io.IOException;

public class PDFLineBreak extends PDFComponent {
    public PDFLineBreak(PDFont font, int size) {
        super(font, size, "_________________________________________");
    }

    public void draw(PDPageContentStream contentStream, int x, int y) throws IOException{
        drawText(contentStream, x, y);
    }

}
