package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFComposite;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class PDFComponent {
    PDFont font;
    int size;
    int x;
    int y;
    String text;

    public List<PDFComponent> children;

    public PDFComponent(PDFont font,  int size, String text) {
        this.children = new ArrayList<PDFComponent>();

        this.font = font;
        this.size = size;
        this.text = text;
    }

    public PDFComponent(int x, int y) {
        this.children = new ArrayList<PDFComponent>();
        this.x = x;
        this.y = y;
    }

    public PDFComponent() {
        this.children = new ArrayList<PDFComponent>();
        this.x = x;
        this.y = y;
    }

    abstract public void draw(PDPageContentStream contentStream, int x, int y) throws IOException;

    public void draw(PDPageContentStream contentStream) throws IOException {
        draw(contentStream, this.x, this.y);
    }


    public void drawText(PDPageContentStream contentStream, int width, int height)
            throws IOException {
        contentStream.beginText();
        contentStream.setFont( font, size);
        contentStream.moveTextPositionByAmount( width, height );
        contentStream.drawString( text );
        contentStream.endText();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
