package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFBuilder;

import nl.avans.ivh11.Facturering.InvoiceGenerator.InvoicePDFGenerator;

import java.io.IOException;

public class PDFBuilder implements  PDFBuilderInterface {
    private InvoicePDFGenerator pdfGenerator;

    private int smallTextSize = 10;
    private int defaultTextSize = 12;
    private int largeTextSize = 20;
    private int maxSessionsPerPage = 23;


    @Override
    public InvoicePDFGenerator getResult() throws IOException {
        return new InvoicePDFGenerator(maxSessionsPerPage, smallTextSize, defaultTextSize, largeTextSize);
    }

    public void setSmallTextSize(int smallTextSize) {
        this.smallTextSize = smallTextSize;
    }

    public void setDefaultTextSize(int defaultTextSize) {
        this.defaultTextSize = defaultTextSize;
    }

    public void setLargeTextSize(int largeTextSize) {
        this.largeTextSize = largeTextSize;
    }

    public void setMaxSessionsPerPage(int maxSessionsPerPage) {
        this.maxSessionsPerPage = maxSessionsPerPage;
    }
}
