package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFComposite;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

import java.io.IOException;

public class PDFText extends PDFComponent {

    public PDFText(PDFont font, int size, String text) {
        super(font, size, text);
    }


    public void draw(PDPageContentStream contentStream, int x, int y) throws IOException {
        drawText(contentStream, x, y);
    }
}
