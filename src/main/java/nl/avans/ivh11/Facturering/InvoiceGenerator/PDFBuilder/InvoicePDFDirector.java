package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFBuilder;

import nl.avans.ivh11.Facturering.InvoiceGenerator.InvoicePDFGenerator;

import java.io.IOException;

public class InvoicePDFDirector {
    private PDFBuilderInterface builder;

    public InvoicePDFDirector(PDFBuilderInterface builder) {
        this.builder = builder;
    }

    public InvoicePDFGenerator Construct() throws IOException{
        builder.setSmallTextSize(10);
        builder.setDefaultTextSize(12);
        builder.setLargeTextSize(20);
        builder.setMaxSessionsPerPage(23);

        return builder.getResult();
    }
}
