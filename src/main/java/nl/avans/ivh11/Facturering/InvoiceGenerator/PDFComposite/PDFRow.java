package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFComposite;

import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import java.io.IOException;

public class PDFRow extends PDFComponent {
    int xSpacingPerChild;

    public PDFRow(int xSpacingPerChild) {
        this.xSpacingPerChild = xSpacingPerChild;
    }

    public void draw(PDPageContentStream contentStream, int _x, int _y) throws IOException{

        int x = _x;
        int y = _y;

        for (PDFComponent child : children) {

            child.draw(contentStream, x, y);
            x += xSpacingPerChild;
        }
    }


    public void addChild(PDFComponent child) {
        this.children.add(child);
    }
}
