package nl.avans.ivh11.Facturering.InvoiceGenerator;

import org.apache.log4j.Logger;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;

import java.io.IOException;
import java.io.OutputStream;


public class PDFGenerator {
    private static final Logger LOGGER = Logger.getLogger(PDFGenerator.class);

    private PDDocument document;
    
    public PDFGenerator() throws IOException{
        document = new PDDocument();
    }

    public PDDocument getDocument() {
        return document;
    }
    
    public void drawText(PDPageContentStream contentStream, PDFont font, 
            int size, int width, int height, String text) 
            throws IOException {
        contentStream.beginText();
        contentStream.setFont( font, size);
        contentStream.moveTextPositionByAmount( width, height ); 
        contentStream.drawString( text );
        contentStream.endText();
    }
    
    public void saveAndClose(OutputStream out) {
        try{
            document.save(out);
            document.close();
        } catch (IOException | COSVisitorException ex){
            LOGGER.error("Unable to save and close document", ex);
        }
    }
}
