package nl.avans.ivh11.Facturering.InvoiceGenerator.PDFBuilder;

import nl.avans.ivh11.Facturering.InvoiceGenerator.InvoicePDFGenerator;

import java.io.IOException;

public interface PDFBuilderInterface {
    public InvoicePDFGenerator getResult() throws IOException;

    public void setSmallTextSize(int smallTextSize);
    public void setDefaultTextSize(int defaultTextSize);
    public void setLargeTextSize(int largeTextSize);
    public void setMaxSessionsPerPage(int maxSessionsPerPage);
}
