/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.avans.ivh11.Facturering.domain.invoice.observer;

import nl.avans.ivh11.Facturering.domain.invoice.observer.InvoiceObserver;
import nl.avans.ivh11.Facturering.domain.invoice.Invoice;

/**
 *
 * @author stefa
 */
public class InvoiceLogObserver extends InvoiceObserver {
    public InvoiceLogObserver(Invoice invoice) {
        this.invoice = invoice;
        this.invoice.attach(this);
    }

    @Override
    public void update() {
        //update log
        System.out.println("[Invoice Log Observer] Log updated: " + invoice.getName() + " state changed to '" + invoice.getInvoiceState().toString() + "'");
    }
}
