package nl.avans.ivh11.Facturering.domain.insurance.discountstrategy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.rmi.UnexpectedException;

@Entity
public abstract class DiscountStrategy {

        @Id
        @GeneratedValue
        private Long id;

        private String name;

        public abstract int GetDiscountPercentage();

        public Long getId() {
                return id;
        }

        public void setId(Long id) {
                this.id = id;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }
}
