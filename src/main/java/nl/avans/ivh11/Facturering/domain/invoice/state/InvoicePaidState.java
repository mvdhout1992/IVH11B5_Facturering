/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.avans.ivh11.Facturering.domain.invoice.state;

import nl.avans.ivh11.Facturering.domain.invoice.state.InvoiceState;
import nl.avans.ivh11.Facturering.domain.invoice.Invoice;

/**
 *
 * @author Stefan
 */
public class InvoicePaidState implements InvoiceState {
    @Override
    public void doAction(Invoice invoice) {
        System.out.println("The invoice is paid");
        invoice.setInvoiceState(this);	
    }

    @Override
    public String toString(){
        return "Invoice paid";
    }
}
