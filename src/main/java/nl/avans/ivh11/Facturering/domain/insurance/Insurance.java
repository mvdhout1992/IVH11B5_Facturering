package nl.avans.ivh11.Facturering.domain.insurance;

import nl.avans.ivh11.Facturering.domain.customer.Address;
import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.DiscountStrategy;
import nl.avans.ivh11.Facturering.domain.insurancecompany.InsuranceCompany;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import org.apache.log4j.Logger;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
// @Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class Insurance {
    //the logger for the class
    private static final Logger LOGGER = Logger.getLogger(Insurance.class);
    //the Hash for the hashcode
    private static final int HASH = 5;
    //the multiplier for the hash
    private static  final int HASH_MULTIPLIER = 61;

    @Id
    @GeneratedValue
    public Long id;
    @NotNull
    @Size(min = 1, max = 32, message = "Naam moet tussen de 1 en 32 karakters bevatten.")
    public String name;
    @NotNull
    @OneToOne
    public InsuranceCompany insuranceCompany;
    @ManyToMany(cascade=CascadeType.MERGE)
    public List<Treatment> includedTreatments;

    public boolean hasPackages = false;
    public boolean isBase = true;

    @Transient
    @ManyToOne(cascade=CascadeType.MERGE)
    public DiscountStrategy discountStrategy;

    public int discountPercentage;

    /**
     * Constructor for the class.
     * <p>
     * Intentional empty constructor for thymeleaf
     */
    public Insurance() {
        this.includedTreatments = new ArrayList<>();
        //intentional empty constructor for thymeleaf
    }

    /**
     * Constructor to create a insurance with a name and insurance company.
     * @param name The name for the insurance
     * @param ic The insurance company the insurance is linked to
     */
    public Insurance(String name, InsuranceCompany ic, DiscountStrategy discountStrategy) {
        this.name = name;
        this.insuranceCompany = ic;
        this.discountStrategy = discountStrategy;

        if (discountStrategy != null)
            this.discountPercentage = discountStrategy.GetDiscountPercentage();
        else
            this.discountPercentage = 0;

        LOGGER.debug("Constructor called for insurance: " + toString());
    }

    Insurance(String testName, Address testAdress, String string, String nL999999999B77, String testBank, String test1emailnl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return The name of the insurance
     */
    public String getName(){
        return name;
    }

    /**
     * @param name The name of the insurance
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The insurance company of the insurance
     */
    public InsuranceCompany getInsuranceCompany() {
        return insuranceCompany;
    }

    /**
     * @param insuranceCompany The insurance company for the insurance
     */
    public void setInsuranceCompany(InsuranceCompany insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    /**
     * @return The id of the insurance
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id The id for the insurance
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The included treatments of the insurance
     */
    public List<Treatment> getIncludedTreatments (){
        return includedTreatments;
    }

    /**
     * @param includedTreatments The included treatments for the insurance
     */
    public void setIncludedTreatments(List<Treatment> includedTreatments) {
        this.includedTreatments = includedTreatments;
    }

    /**
     * Adds included treatments to the insurance.
     * @param treatments The treatment to add
     */
    public void addIncludedTreatments(List<Treatment> treatments){
        includedTreatments.addAll(treatments);
    }

    public int getDiscountPercentage(){
        return this.discountPercentage;
    }

    public void setDiscountPercentage(int discount){
        this.discountPercentage = discount;
    }

    public DiscountStrategy getDiscountStrategy() {
        return discountStrategy;
    }

    public void setDiscountStrategy(DiscountStrategy discountStrategy) {
        this.discountStrategy = discountStrategy;
    }

    public int hashCode() {
        int hash = HASH;
        hash = HASH_MULTIPLIER * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Insurance other = (Insurance) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "Insurance{" + "name=" + name + ", includedTreatments=" +
                includedTreatments + '}';
    }
}
