package nl.avans.ivh11.Facturering.domain.insurance.discountstrategy;

import javax.persistence.Entity;

@Entity
public class NewCustomerDiscount extends DiscountStrategy {

    public NewCustomerDiscount(){
        this.setName("New customer discount");
    }

    @Override
    public int GetDiscountPercentage() {
        return 5;
    }
}
