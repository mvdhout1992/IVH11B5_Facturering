/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.avans.ivh11.Facturering.domain.invoice.observer;

import nl.avans.ivh11.Facturering.domain.invoice.Invoice;

/**
 *
 * @author stefa
 */
public abstract class InvoiceObserver {

    protected Invoice invoice;

    public abstract void update();
}

