package nl.avans.ivh11.Facturering.domain.customer;

import nl.avans.ivh11.Facturering.domain.invoice.Invoice;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.domain.insurance.Insurance;
import org.apache.log4j.Logger;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * The model for a customer.
 * <p>
 * It houses the information necessary only for the payment system about a customer. This extends from {@link PublicCustomer}
 */
@Entity
public class Customer extends PublicCustomer{
    //The logger of this class
    private static final Logger LOGGER = Logger.getLogger(Customer.class);
    //the hash(used in hashcode)
    private static final int HASH = 5;
    //the hash multiplier(used in hashcode)
    private static final int HASH_MULTIPLIER = 29;
    //The VAT rate (in numbers not percent)
    private static final double VAT = 1.19;


    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private Gender gender;

    // TODO MJRAIJMA
    @NotNull
    // @ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.MERGE, targetEntity = Insurance.class)
    @ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.MERGE)
    private Insurance insurance;
    
    @NotNull
    private int deductables;


    public boolean dentalPackage = false;
    public boolean fysioPackage = false;

    /**
     * The class constructor.
     * <p>
     * Intentional empty constructor for thymeleaf
     */
    public Customer() {
        super();
        //intentional for thymeleaf
    }

    /**
     * The constructor of the class to initialise all attributes.
     * <p>
     * Initialises all attributes and makes a {@link PublicCustomer}
     * @param bsn The customer's service number
     * @param name The {@link Name} of the customer
     * @param address The {@link Address} of the customer
     * @param birthDate The customer's birth date
     * @param phoneNumber The customer's phone number
     * @param email The customer's email address
     * @param gender The customer's gender, MALE or FEMALE
     * @param insurance The insurance the customer has
     */
    public Customer(String bsn, Name name, Address address,
                    Date birthDate, String phoneNumber, String email, Gender gender,
                    Insurance insurance, int deductables) {
        super(bsn, name, address, birthDate, phoneNumber, email);
        
        this.gender = gender;
        this.insurance = insurance;
        this.deductables = deductables;
        LOGGER.debug("Constructor called for customer: " + this.toString());
    }

    /**
     * @return The customer's id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id The id to set the customer's id to
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return A Gender enum, MALE or FEMALE
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * @param gender MALE or FEMALE
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * @return The insurance the customer has
     */
    public Insurance getInsurance() {
        return insurance;
    }

    /**
     * @param insurance The insurance to bind to the customer
     */
    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }
    
    public int getDeductables(){
        return deductables;
    }
    
    public void setDeductables(int deductables){
        this.deductables = deductables;
    }

    public boolean isDentalPackage() {
        return dentalPackage;
    }

    public void setDentalPackage(boolean dentalPackage) {
        this.dentalPackage = dentalPackage;
    }

    public boolean isFysioPackage() {
        return fysioPackage;
    }

    public void setFysioPackage(boolean fysioPackage) {
        this.fysioPackage = fysioPackage;
    }

    public void calculateDeductables(Invoice invoice){
        // maak een lijst met daarin de treatments die op de factuur voorkomen
        // vervolgens van die lijst alleen de treatments houden die ook in de includedtreatments voorkomen
        List<Treatment> lt = new ArrayList<Treatment>(invoice.getTreatments());
        lt.retainAll(insurance.getIncludedTreatments());
        
        //voor elk insurance in de lijst met treatments het subtotaal ophalen en bij elkaar optellen
        for (Treatment t : lt) {
            deductables = (int) (deductables - (t.getSubTotal().intValue())* VAT);
        }
    }
    
    @Override
    public int hashCode() {
        int hash = HASH;
        hash = HASH_MULTIPLIER * hash + Objects.hashCode(getBsn());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PublicCustomer other = (PublicCustomer) obj;
        return Objects.equals(getBsn(), other.getBsn());
    }

    @Override
    public String toString() {
        return super.toString() + " Customer{" + "id=" + id + " gender=" + gender +
                ", insurance=" + insurance + "}";
    }
}

/**
 * The <code>Gender</code> of a customer.
 * <p>
 * MALE for male<br>
 * FEMALE for female
 */


