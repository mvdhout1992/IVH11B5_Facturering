package nl.avans.ivh11.Facturering.domain.interpreter;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;

import java.util.List;

public class AndExpression implements Expression {
    private Expression expr1 = null;
    private Expression expr2 = null;

    public AndExpression(Expression expr1, Expression expr2) {
        this.expr1 = expr1;
        this.expr2 = expr2;
    }

    @Override
    public String toString() {
        return "AndExpression{\n/           \\\n" +
                "expr1=" + expr1.toString() +
                ", expr2=" + expr2.toString() +
                '}';
    }

    public Expression getExpr1() {
        return expr1;
    }

    public Expression getExpr2() {
        return expr2;
    }

    @Override
    public List<Treatment> interpret(List<Treatment> treatments) {
        List<Treatment> exprList1 = expr1.interpret(treatments);
        List<Treatment> exprList2 = expr2.interpret(treatments);

        exprList1.retainAll(exprList2);
        return exprList1;
    }
}
