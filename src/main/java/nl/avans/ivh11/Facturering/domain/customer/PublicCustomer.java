package nl.avans.ivh11.Facturering.domain.customer;

import org.apache.log4j.Logger;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * Public info for Customer, for consumption by the other project group via REST
 * JSON API this system hosts
 * 
 */
@MappedSuperclass
public class PublicCustomer {
    //The logger of the class
    private static final Logger LOGGER = Logger.getLogger(PublicCustomer.class);

    @NotNull(message = "BSN kan niet null zijn")
    @Pattern(regexp = "\\d{9}", message = "BSN moet 9 cijfers bevatten")
    private String bsn;
    @NotNull
    @Valid
    @OneToOne
    private Name name;
    @NotNull
    @Valid
    //@Access(AccessType.PROPERTY)
    //@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    // @JoinColumn(name="userId")
    @OneToOne
    private Address address;
    // Change the way the date is displayed by Spring's web form handling
    @NotNull(message = "Geboortedatum moet ingevuld zijn")
    @DateTimeFormat(iso= ISO.DATE)
    @Past(message = "Datum moet in het verleden liggen")
    private Date birthDate;
    @NotNull
    @Pattern(regexp = "\\d{10}", message = "Telefoonnummer moet 10 cijfers bevatten")
    private String phoneNumber;
    @NotNull
    @Pattern(regexp = "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$", message = "emailaddress moet correct zijn.")
    private String email;

    /**
     * The class constructor.
     * <p>
     * intentional empty constructor for thymeleaf
     */
    public PublicCustomer(){
        //Intentional for thymeleaf
    }

    /**
     * The constructor of the class witch all attributes.
     * <p>
     * makes a new <code>PublicCustomer</code> with all the attributes as parameters
     *
     * @param bsn The customer's service number
     * @param name The name of the customer
     * @param address The address of the customer
     * @param birthDate The birthdate of the customer
     * @param phoneNumber The customer's phonenumber
     * @param email Customer's email
     */
    public PublicCustomer(String bsn, Name name, Address address,
                          Date birthDate, String phoneNumber, String email) {
        this.bsn = bsn;
        this.name = name;
        this.address = address;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    /**
     * The constructor of the class with a customer.
     * <p>
     * Makes a new <code>PublicCustomer</code> with a customer object
     * @param c the customer you want to make a PublicCustomer of
     */
    public PublicCustomer(Customer c) {
        this.bsn = c.getBsn();
        this.name = c.getName();
        this.address = c.getAddress();
        this.birthDate = c.getBirthDate();
        this.phoneNumber = c.getPhoneNumber();
        this.email = c.getEmail();
    }


    /**
     * @return the customer's service number
     */
    public String getBsn() {
        return bsn;
    }

    /**
     * @param bsn the bsn to set the customer's bsn to
     */
    public void setBsn(String bsn) {
        this.bsn = bsn;
    }

    /**
     * @return A {@link Name} object specifying the name of the customer
     */
    public Name getName(){
        return name;
    }

    /**
     * @param name a {@link Name} object specifying the customer's name
     */
    public void setName(Name name){
        this.name = name;
    }

    /**
     * @return an {@link Address} object containing the customer's address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address an {@link Address} object containing the customer's address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @return the birth date of the customer
     */
    public Date getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate the date to set the customer's birthdate to
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @return The customer's phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phone number to set the customer's phone number to
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the cusomer's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set the customer's email to
     */
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "PublicCustomer{" + "bsn=" + bsn + ", name" + name +
                ", address=" + address + ", birthDate=" + birthDate + ", phoneNumber=" + phoneNumber
                + ", email=" + email;
    }
    
    
}
