package nl.avans.ivh11.Facturering.domain.insurance.decorator;

import nl.avans.ivh11.Facturering.domain.insurance.Insurance;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class DentalPackageDecorator extends InsurancePackageDecorator {

    public DentalPackageDecorator(Insurance insurance) {
        super(insurance);
    }
}