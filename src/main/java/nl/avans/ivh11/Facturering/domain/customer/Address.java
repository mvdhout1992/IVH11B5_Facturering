package nl.avans.ivh11.Facturering.domain.customer;

import org.apache.log4j.Logger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * The address, contains street, zip and city
 */
@Entity
public class Address{
    //the logger for the class
    private static final Logger LOGGER = Logger.getLogger(Address.class);

    @Id
    @GeneratedValue
    private Long id;

    @NotNull(message = "Adres kan geen null zijn")
    @Pattern(regexp = "^[A-Za-z]+[ ][1-9][0-9]*[a-z]?$", message = "Adres moet het formaat \'straatnaam 19a\' hebben")
    private String street;

    @NotNull(message = "Postcode kan niet null zijn")
    @Pattern(regexp = "^[1-9][0-9]{3}[A-Z]{2}$", message = "Postcode moet het formaat \'1234AB\' hebben en kan niet met 0 beginnen")
    private String zipCode;

    @NotNull(message = "Stad kan niet null zijn")
    @Size(min = 1, max = 64, message = "Plaatsnaam kan niet langer zijn dan 64 karakters")
    private String city;

    /**
     * Constructor for the class.
     * <p>
     * Intentional empty constructor for thymeleaf
     *
     */
    public Address(){
        //Intentional for thymeleaf
    }

    /**
     * Constructor for the class to make an address with all parameters.
     * <p>
     * Zipcodes can only be dutch zipcodes
     * @param street The street and house number
     * @param zipCode The zipcode in dutch format
     * @param city The city
     */
    public Address(String street, String zipCode, String city){
        this.street = street;
        this.zipCode = zipCode;
        this.city = city;
        LOGGER.debug("constructor called for address: " + toString());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return The street and house number
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street The street and house number
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The zipcode
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * @param zipCode The zipcode
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city
     */
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{" + "street=" + street + ", zipCode=" + zipCode + ", city=" + city + "}";
    }
}
