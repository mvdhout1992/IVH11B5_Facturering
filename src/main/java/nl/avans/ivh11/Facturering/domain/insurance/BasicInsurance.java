package nl.avans.ivh11.Facturering.domain.insurance;

import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.DiscountStrategy;
import nl.avans.ivh11.Facturering.domain.insurancecompany.InsuranceCompany;

import javax.persistence.Entity;

@Entity
public class BasicInsurance extends Insurance {

    public BasicInsurance(){
        super();
    }

    public BasicInsurance(String name, InsuranceCompany ic, DiscountStrategy discountStrategy) {
        super(name, ic, discountStrategy);
    }
}
