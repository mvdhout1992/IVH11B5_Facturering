package nl.avans.ivh11.Facturering.domain.treatment.Memento;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;

public class TreatmentOriginator {
    private Treatment treatment;

    public void set(Treatment newtreatment){
        System.out.print("From Originator: Current version of memento: " + newtreatment + "\n");
        this.treatment = newtreatment;
    }

    public TreatmentMemento storeInMemento(){
        System.out.print("From Originator: Saving to Memento \n");
        return new TreatmentMemento(treatment);
    }

    public Treatment restoreFromMomento(TreatmentMemento memento){
        this.treatment = memento.getSavedTreatment();
        System.out.print("From Originator: Previous treatment saved in memento \n" + treatment);
        return treatment;
    }
}
