package nl.avans.ivh11.Facturering.domain.invoice;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.domain.customer.Customer;
import org.apache.log4j.Logger;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import nl.avans.ivh11.Facturering.domain.invoice.observer.InvoiceObserver;
import nl.avans.ivh11.Facturering.domain.invoice.state.InvoiceState;

@Entity
public class Invoice {
    //The logger of the class
    private static final Logger LOGGER = Logger.getLogger(Invoice.class);
    //The hash for the hashcode
    private static final int HASH = 5;
    //The hashmultiplier for the hashcode
    private static final int HASH_MULTIPLIER = 67;

    @GeneratedValue
    @Id
    private Long id;
    @Size(min = 1, max = 16, message = "Naam moet tussen de 1 en 16 karakters zijn")
    private String name;
    @OneToOne
    private Customer customer;
    @ManyToMany
    private List<Treatment> treatments;
    // Change the way the date is displayed by Spring's web form handling
    @NotNull(message = "datum moet ingevuld zijn")
    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    private Date date;
    @NotNull(message = "datum moet ingevuld zijn")
    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    private Date expirationDate;
    private boolean hasPaid;
    
    @Transient
    private InvoiceState invoiceState;
    
    @Transient
    private List<InvoiceObserver> invoiceObservers = new ArrayList<InvoiceObserver>();

    public void attach(InvoiceObserver invoiceObserver) {
        invoiceObservers.add(invoiceObserver);
    }

    public void notifyAllObservers() {
        for (InvoiceObserver invoiceObserver : invoiceObservers) {
            invoiceObserver.update();
        }
    }

    public void setInvoiceState(InvoiceState invoiceState) {
        this.invoiceState = invoiceState;
        notifyAllObservers();		
    }

    public InvoiceState getInvoiceState() {
        return invoiceState;
    }


    /**
     * The constructor of the class.
     * <p>
     * intentional empty constructor for thymeleaf
     */
    public Invoice() {
        this.treatments = new ArrayList<>();
        //intentional for thymeleaf
    }

    /**
     * The constructor to initialise an invoice with all attributes.
     * @param name The name of the invoice
     * @param customer The customer the invoice is for
     * @param date The date the invoice is made
     * @param expirationDate The date the invoice has to be paid
     */
    public Invoice(String name, Customer customer, Date date, Date expirationDate) {
        this.name = name;
        this.customer = customer;
        this.treatments = new ArrayList<>();
        this.date = date;
        this.expirationDate = expirationDate;
        this.invoiceState = null;
        this.hasPaid = false;
        LOGGER.debug("Constructor called for invoice: " + toString());
    }

    /**
     * @return The customer the invoice is for
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * @return The amount of treatments on the invoice
     */
    public int getTreatmentsCount() {
        return this.treatments.size();
    }

    /**
     * @param customer The customer the invoice is for
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * @return A list of treatments on the invoice
     */
    public List<Treatment> getTreatments() {
        return treatments;
    }

    /**
     * @return The amount of sessions the customer had
     */
    public int getSessionsCount() {
        return this.getTreatments().size();
    }

    /**
     * Calculates the subtotal of all the treatments on the invoice.
     * @return The price of the treatments excluding taxes
     */
    public BigDecimal calculateSubtotal() {
        BigDecimal subtotal = new BigDecimal(0);
        
        for (Treatment t : this.getTreatments() ) {
            subtotal = subtotal.add(t.getSubTotal());
        }
        
        return subtotal;
    }

    /**
     * calculates the total price of the invoice including VAT.
     * <p>
     * adds the VAT to the subtotal
     *
     * @param vat the VAT percentage
     * @return the price of the invoice including taxes
     */
    public BigDecimal calculateTotal(BigDecimal vat) {
        return calculateSubtotal().add(calculateVAT(vat));
    }

    /**
     * Calculates the vat of the invoice
     * @param vat the vat
     * @return The total taxes on the subtotal
     */
    public BigDecimal calculateVAT(BigDecimal vat) {
        return calculateSubtotal().multiply(vat);
    }

    /**
     * @param treatments a list of treatments to set the list to
     */
    public void setTreatments(List<Treatment> treatments) {
        this.treatments = treatments;
    }

    /**
     * Adds a insurance to the invoice.
     * @param t The insurance to add
     */
    public void addTreatment(Treatment t) {
        this.treatments.add(t);
    }

    /**
     * @return The date the invoice is made
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date the date the invoice is made
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return The date the invoice has to be paid
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate  The date the invoice has to be paid
     */
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return The id of the invoice
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id of the invoice
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name of the invoice
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name of the invoice
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the payment status of the invoice
     */
    public boolean getHasPaid() {
        return hasPaid;
    }

    /**
     * @param hasPaid The payment status of the invoice
     */
    public void setHasPaid(boolean hasPaid) {
        this.hasPaid = hasPaid;
    }

    @Override
    public int hashCode() {
        int hash = HASH;
        hash = HASH_MULTIPLIER * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Invoice other = (Invoice) obj;
        return Objects.equals(this.name, other.name);
    }



    @Override
    public String toString() {
        return "INVOICE{" + "id=" + id + ", name=" + name + ", customer=" + customer +
                ", treatments=" + treatments + ", date=" + date + 
                ", expirationDate=" + expirationDate + '}';
    }
  
}
