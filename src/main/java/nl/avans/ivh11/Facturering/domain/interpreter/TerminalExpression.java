package nl.avans.ivh11.Facturering.domain.interpreter;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;

import java.util.ArrayList;
import java.util.List;

public class TerminalExpression implements Expression {

    private AttributeValue data;

    public TerminalExpression(AttributeValue data){
        this.data = data;
    }

    @Override
    public String toString() {
        return "TerminalExpression{" +
                "data='" + data + '\'' +
                '}';
    }

    public String getData() {
        return data.getValue();
    }

    @Override
    public List<Treatment> interpret(List<Treatment> treatments) {
        List<Treatment> returnList = new ArrayList<Treatment>();

        for(Treatment t : treatments) {

            if (data.attribute.equals("name")) {
                if (t.getName().equals(data.value)) {
                    returnList.add(t);
                }
            }

            if (data.attribute.equals("code")) {
                if (t.getCode().equals(data.value)) {
                    returnList.add(t);
                }
            }
        }

        return returnList;
    }
}