package nl.avans.ivh11.Facturering.domain.insurancecompany;

import nl.avans.ivh11.Facturering.domain.customer.Address;
import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.Email;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
public class InsuranceCompany {
    private static final Logger LOGGER = Logger.getLogger(InsuranceCompany.class);
    private static final int HASH = 7;
    private static final int HASH_MULTIPLIER = 59;

    @Id
    @GeneratedValue
    private Long id;
    @NotNull
    @Size(min = 1, max = 32, message = "Naam moet tussen de 1 en 32 karakters bevatten")
    private String name;
    @NotNull
    @Valid
    @OneToOne
    private Address address;
    @NotNull
    @Pattern(regexp = "\\d{8}", message = "kvk nummer moet uit 8 cijfers bestaan")
    private String kvK;
    @NotNull
    @Pattern(regexp = "(NL)?[0-9]{9}B[0-9]{2}", message = "Belastings nummer moet het formaat \'NL123456789B12\' hebben")
    private String taxNumber;
    @NotNull
    @Size(min = 1, max = 32, message = "bank moet uit 1 tot 32 karakters bestaan")
    private String bank;
    @NotNull
    @Email
    private String email;

    public InsuranceCompany() {
        //intentional for Thymeleaf
    }
    
    public InsuranceCompany(String name, Address address, String kvK, String taxNumber, String bank, String email) {
        this.name = name;
        this.address = address;
        this.kvK = kvK;
        this.taxNumber = taxNumber;
        this.bank = bank;
        this.email = email;
        LOGGER.debug("Constructor called for InsuranceCompany: " + toString());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getKvK() {
        return kvK;
    }

    public void setKvK(String kvK) {
        this.kvK = kvK;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = HASH;
        hash = HASH_MULTIPLIER * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InsuranceCompany other = (InsuranceCompany) obj;
        return Objects.equals(this.name, other.name);
    }

    @Override
    public String toString() {
        return "InsuranceCompany{" + "name=" + name + ", address=" + address + 
                ", kvK=" + kvK +
                ", taxNumber=" + taxNumber + ", bank=" + bank + ", email=" + 
                email + '}';
    }
  
  
  
}







