package nl.avans.ivh11.Facturering.domain.insurance.discountstrategy;

import javax.persistence.Entity;

@Entity
public class StandardDiscount extends DiscountStrategy {

    public StandardDiscount(){
        this.setName("No discount");
    }

    @Override
    public int GetDiscountPercentage() {
        return 0;
    }
}
