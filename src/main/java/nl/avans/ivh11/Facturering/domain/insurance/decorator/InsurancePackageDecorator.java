package nl.avans.ivh11.Facturering.domain.insurance.decorator;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.domain.insurance.Insurance;

import java.util.List;

public abstract class InsurancePackageDecorator extends Insurance {
    public Insurance insurance;
    public List<Treatment> packageTreatments;

    public InsurancePackageDecorator(Insurance insurance){
        this.insurance = insurance;
    }

    public void AddTreatments(){
        this.insurance.addIncludedTreatments(packageTreatments);
    }

    public Insurance getInsurance(){
        return this.insurance;
    }
}
