package nl.avans.ivh11.Facturering.domain.insurance.decorator;

import nl.avans.ivh11.Facturering.domain.insurance.Insurance;

import javax.persistence.Entity;

@Entity
public class FysioPackageDecorator extends InsurancePackageDecorator {

    public FysioPackageDecorator(Insurance insurance){
        super (insurance);
    }
}
