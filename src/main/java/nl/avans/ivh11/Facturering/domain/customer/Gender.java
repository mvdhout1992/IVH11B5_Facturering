package nl.avans.ivh11.Facturering.domain.customer;

public enum Gender {
    MALE("MALE", "man"),
    FEMALE("FEMALE", "vrouw");

    private String value;
    private String msg;

    Gender(String value, String msg){
        this.value = value;
        this.msg = msg;
    }

    /**
     * @return The name value of the enum
     */
    public String getValue(){
        return value;
    }

    /**
     * @return The text message for the enum
     */
    public String getMsg(){
        return msg;
    }
}