package nl.avans.ivh11.Facturering.domain.treatment.Memento;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Entity
public class TreatmentCaretaker {
    @ManyToMany(fetch = FetchType.EAGER)
  private  List<TreatmentMemento> SavedTreatments;
    @ManyToMany(fetch = FetchType.LAZY)
  private  List<TreatmentMemento> lijst;
    @GeneratedValue
  @Id
  Long id;

    public TreatmentCaretaker() {
        this.lijst = new ArrayList<TreatmentMemento>();
        this.SavedTreatments = new ArrayList<TreatmentMemento>();
    }

    //    public int getCurrentarticle(){
//        return currentarticle;
//    }
//
//    public void setCurrentArticle(Boolean m){
//
//        if(m == false){
//            if(currentarticle >= 1){
//                currentarticle--;
//            }
//        }else if(m == true){
//            if((savefiles-1) > currentarticle){
//                currentarticle++;
//            }
//        }
//    }

    public void addmemento(TreatmentMemento m){
        SavedTreatments.add(m);
        for(TreatmentMemento a : SavedTreatments){
            System.out.print("saved memento in savedtreatment list:  "+ a.getSavedTreatment() + " \n");
        }

    }

    public int getSize(){
        return lijst.size();
    }

    public TreatmentMemento getMemento(int index){

       return lijst.get(index);
    }

    public List<TreatmentMemento> getSavedTreatments() {
        return SavedTreatments;
    }

    public void setSavedTreatments(List<TreatmentMemento> savedTreatments) {
        SavedTreatments = savedTreatments;
    }

    public List<TreatmentMemento> getLijst() {
        return lijst;
    }

    public void setLijst(List<TreatmentMemento> lijst) {
        this.lijst = lijst;
    }

    public void addListToLijst(List<TreatmentMemento> list2) {
        this.lijst.addAll(list2);
    }

    public void addListToSavedTreatments(List<TreatmentMemento> list2) {
        this.SavedTreatments.addAll(list2);
    }

    public void GenerateNewList(int id){
        lijst.clear();
        for(int i = 0; i< SavedTreatments.size(); i++){
            if(SavedTreatments.get(i).getSavedTreatment().getId() == id){
            lijst.add(SavedTreatments.get(i));
            }
        }
        for(TreatmentMemento memento : lijst){
            System.out.print("added: to list: " + memento.getSavedTreatment() + "\n" );
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
