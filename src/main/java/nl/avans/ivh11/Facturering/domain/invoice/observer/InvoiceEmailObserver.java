/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.avans.ivh11.Facturering.domain.invoice.observer;

import nl.avans.ivh11.Facturering.domain.invoice.Invoice;

/**
 *
 * @author stefa
 */
public class InvoiceEmailObserver extends InvoiceObserver {
    public InvoiceEmailObserver(Invoice invoice) {
        this.invoice = invoice;
        this.invoice.attach(this);
    }

    @Override
    public void update() {
        //send email
        System.out.println("[Invoice Email Observer] Email sent to: '" + invoice.getCustomer().getEmail() + "'");
    }
}