package nl.avans.ivh11.Facturering.domain.interpreter;

public class AttributeValue {
    String attribute;
    String value;

    public AttributeValue(String attribute, String value) {
        this.attribute = attribute;
        this.value = value;
    }

    public String getAttribute() {
        return attribute;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "AttributeValue{" +
                "attribute='" + attribute + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
