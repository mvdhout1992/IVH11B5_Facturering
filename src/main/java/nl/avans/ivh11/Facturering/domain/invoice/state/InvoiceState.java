/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.avans.ivh11.Facturering.domain.invoice.state;

import nl.avans.ivh11.Facturering.domain.invoice.Invoice;

/**
 *
 * @author Stefan
 */
public interface InvoiceState {
    public void doAction(Invoice invoice);
}
