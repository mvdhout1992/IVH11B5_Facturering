package nl.avans.ivh11.Facturering.domain.insurance.discountstrategy;

import javax.persistence.Entity;

@Entity
public class ReturningCustomerDiscount extends DiscountStrategy {

    public ReturningCustomerDiscount(){
        this.setName("Returning customer discount");
    }

    @Override
    public int GetDiscountPercentage() { return 8; }
}
