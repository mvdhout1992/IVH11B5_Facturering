package nl.avans.ivh11.Facturering.domain.interpreter;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import java.util.List;

public interface Expression {
    public List<Treatment> interpret(List<Treatment> treatments);
}
