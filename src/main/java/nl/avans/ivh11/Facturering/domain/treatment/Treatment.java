package nl.avans.ivh11.Facturering.domain.treatment;

import org.apache.log4j.Logger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * The model for a <code>Treatment</code>, witch contains all its attributes
 */
@Entity
public class Treatment{
    //the logger for the class
    private static final Logger LOGGER = Logger.getLogger(Treatment.class);

    //The hash of the hashcode
    private static final int HASH = 7;
    private static final int HASH_MULTIPLIER = 17;

    @Id
    @GeneratedValue
    private Long id;
    private String code;
    private String name;
    private BigDecimal sessionsAmount;
    private String sessionLength;
    private BigDecimal priceRate;

    /**
     * The constructor of the class.
     * <p>
     * Intentional empty constructor for thymeleaf
     */
    public Treatment() {
        //intentional for thymeleaf
    }

    /**
     * Constructor to initialise all the attributes for a <code>Treatment</code>
     * @param code The insurance code
     * @param name The name of the insurance
     * @param sessionsAmount The amount of sessions included in the insurance
     * @param sessionLength The length of each session
     * @param priceRate The price per hour of each insurance
     */
    public Treatment(String code, String name, BigDecimal sessionsAmount,
            String sessionLength, BigDecimal priceRate) {
        this.code = code;
        this.name = name;
        this.sessionsAmount = sessionsAmount;
        this.sessionLength = sessionLength;
        this.priceRate = priceRate;
        LOGGER.debug("Constructor called for insurance: "+ toString());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the subtotal of the price of the sessions.
     */
    @Transient
    public BigDecimal getSubTotal() {
        return this.getPriceRate().multiply(getTotalSessionLength()); 
    }

    /**
     * @return the price of one session
     */
    public BigDecimal getPriceForSession() {
        return this.getPriceRate().multiply(new BigDecimal(this.getSessionLength()));
    }

    /**
     * @param vAT The VAT percentage
     * @return The totalcosts including VAT
     */
    public BigDecimal getTotal(BigDecimal vAT) {
        return this.getSubTotal().multiply(vAT.add(new BigDecimal("1")));
    }

    /**
     * @return The total time of all sesions combined
     */
    public BigDecimal getTotalSessionLength() {
        return new BigDecimal(sessionLength).multiply(sessionsAmount);
    }

    /**
     * @return The insurance code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code The insurance code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name of the insurance
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name of the insurance
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the amount of session
     */
    public BigDecimal getSessionsAmount() {
        return sessionsAmount;
    }

    /**
     * @param sessionsAmount the amount of session
     */
    public void setSessionsAmount(BigDecimal sessionsAmount) {
        this.sessionsAmount = sessionsAmount;
    }

    /**
     * @return The length of a session in hours
     */
    public String getSessionLength() {
        return sessionLength;
    }

    /**
     * @param sessionLength The length of a session in hours
     */
    public void setSessionLength(String sessionLength) {
        this.sessionLength = sessionLength;
    }

    /**
     * @return return price rate in cents
     */
    public BigDecimal getPriceRate() {
        return priceRate;
    }

    /**
     *
     * @param priceRate The price rate in cents
     */
    public void setPriceRate(BigDecimal priceRate) {
        this.priceRate = priceRate;
    }

    @Override
    public int hashCode() {
        int hash = HASH;
        hash = HASH_MULTIPLIER * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Treatment other = (Treatment) obj;
        return Objects.equals(this.code, other.code);
    }

    @Override
    public String toString() {
        return "Treatment{" + "code=" + code + ", name=" + name + 
                ", sessionsAmount=" + sessionsAmount + ", sessionLength=" + 
                sessionLength + ", priceRate=" + priceRate + '}';
    }

    
    
}
