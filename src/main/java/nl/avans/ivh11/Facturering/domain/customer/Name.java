package nl.avans.ivh11.Facturering.domain.customer;

import org.apache.log4j.Logger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * contains all the information about a person's name.
 */
@Entity
public class Name{
    //The logger for this class
    private static final Logger LOGGER = Logger.getLogger(Name.class);

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(max = 32, message = "Voornaam kan niet langer zijn dan 32 karakters")
    private String firstName;

    @Size(max = 16, message = "Tussenvoegsel kan niet langer zijn dan 16 karakters")
    private String infix;

    @NotNull
    @Size(max = 32, message = "Achternaam kan niet langer zijn dan 32 karakters")
    private String lastName;

    /**
     * Constructor of the class.
     * <p>
     * intentional empty constructor for thymeleaf
     */
    public Name(){
        //Intentional for thymeleaf
    }

    /**
     * Constructor with all parameters for name.
     * @param firstName the firstname
     * @param infix the infix of the name
     * @param lastName the lastname
     */
    public Name(String firstName, String infix, String lastName){
        this.firstName = firstName;
        this.infix = infix;
        this.lastName = lastName;
        LOGGER.debug("constructor called for name: " + toString());
    }

    /**
     * Constructor to create a name without infix
     * <p>
     * Uses the other constructor to set infix to ""
     * @param firstName the firstname
     * @param lastName the lastname
     */
    public Name(String firstName, String lastName){
        this(firstName, "", lastName);
    }

    /**
     * @return the firstname
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the last name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the infix, "" if there is none
     */
    public String getInfix() {
        return infix;
    }

    /**
     * @param infix the infix
     */
    public void setInfix(String infix) {
        this.infix = infix;
    }

    /**
     * @return the lastname
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastname
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString(){
        if("".equals(infix)){
            return firstName + " " + lastName;
        }
        return firstName + " " + infix + " " + lastName;
    }
}
