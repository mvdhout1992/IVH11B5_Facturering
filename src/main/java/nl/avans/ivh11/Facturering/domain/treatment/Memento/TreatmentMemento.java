package nl.avans.ivh11.Facturering.domain.treatment.Memento;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;

import javax.persistence.*;

@Entity
public class TreatmentMemento {
    @OneToOne
    private Treatment treatment;

    public TreatmentMemento() {
    }

    @GeneratedValue
    @Id
    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TreatmentMemento(Treatment treatmentSave) {
        this.treatment = treatmentSave;
    }

    public Treatment getSavedTreatment() {
        return treatment;
    }
}
