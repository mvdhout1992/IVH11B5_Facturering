package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.insurancecompany.InsuranceCompany;
import org.springframework.data.repository.CrudRepository;

public interface InsuranceCompanyRepository extends CrudRepository<InsuranceCompany, Long> {
}