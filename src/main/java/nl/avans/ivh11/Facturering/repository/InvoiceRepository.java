package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.invoice.Invoice;
import org.springframework.data.repository.CrudRepository;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {
}
