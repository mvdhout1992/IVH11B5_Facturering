package nl.avans.ivh11.Facturering.repository;


import nl.avans.ivh11.Facturering.domain.treatment.Memento.TreatmentMemento;
import org.springframework.data.repository.CrudRepository;

public interface MementoRepository extends CrudRepository<TreatmentMemento, Long> {
}
