package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.customer.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
