package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.treatment.*;
import nl.avans.ivh11.Facturering.domain.treatment.Memento.TreatmentCaretaker;
import org.springframework.data.repository.CrudRepository;

public interface CaretakerRepository extends CrudRepository<TreatmentCaretaker, Long> {
}
