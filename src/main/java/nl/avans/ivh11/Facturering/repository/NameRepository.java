package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.customer.Name;
import org.springframework.data.repository.CrudRepository;

public interface NameRepository extends CrudRepository<Name, Long> {
}
