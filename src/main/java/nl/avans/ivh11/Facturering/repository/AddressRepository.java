package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.customer.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {
}

