package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.DiscountStrategy;
import org.springframework.data.repository.CrudRepository;

public interface DiscountStrategyRepository extends CrudRepository<DiscountStrategy, Long> {
}