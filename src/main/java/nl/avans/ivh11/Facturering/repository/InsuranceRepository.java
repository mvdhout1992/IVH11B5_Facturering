package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.insurance.Insurance;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InsuranceRepository extends CrudRepository<Insurance, Long> {
}