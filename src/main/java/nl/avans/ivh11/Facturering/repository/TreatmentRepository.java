package nl.avans.ivh11.Facturering.repository;

import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import org.springframework.data.repository.CrudRepository;

public interface TreatmentRepository extends CrudRepository<Treatment, Long> {
    Treatment getTreatmentByName(String Name);
    Treatment getTreatmentByCode(String Code);
}
