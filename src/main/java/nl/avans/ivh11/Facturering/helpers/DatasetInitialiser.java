package nl.avans.ivh11.Facturering.helpers;

import nl.avans.ivh11.Facturering.domain.*;
import nl.avans.ivh11.Facturering.domain.customer.*;
import nl.avans.ivh11.Facturering.domain.insurance.*;
import nl.avans.ivh11.Facturering.domain.insurance.decorator.DentalPackageDecorator;
import nl.avans.ivh11.Facturering.domain.insurance.decorator.FysioPackageDecorator;
import nl.avans.ivh11.Facturering.domain.insurance.decorator.InsurancePackageDecorator;
import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.DiscountStrategy;
import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.NewCustomerDiscount;
import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.ReturningCustomerDiscount;
import nl.avans.ivh11.Facturering.domain.insurance.discountstrategy.StandardDiscount;
import nl.avans.ivh11.Facturering.domain.insurancecompany.InsuranceCompany;
import nl.avans.ivh11.Facturering.domain.invoice.Invoice;
import nl.avans.ivh11.Facturering.domain.treatment.Memento.TreatmentCaretaker;
import nl.avans.ivh11.Facturering.domain.treatment.Memento.TreatmentMemento;
import nl.avans.ivh11.Facturering.domain.treatment.Treatment;
import nl.avans.ivh11.Facturering.repository.*;
import nl.avans.ivh11.Facturering.service.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DatasetInitialiser {

    private CustomerService customerService;
    private InsuranceCompanyService insuranceCompanyService;
    private InsuranceService insuranceService;
    private InvoiceService invoiceService;
    private TreatmentService treatmentService;
    private AddressRepository addressRepository;
    private NameRepository nameRepository;
    private DiscountStrategyRepository discountStrategyRepository;
    private TreatmentCaretakerService treatmentCaretakerService;
    private TreatmentMementoService treatmentMementoService;

    private InsuranceCompany ic;
    private Insurance i1;
    private Insurance i2;
    private Insurance i3;
    private Insurance i4;
    private Customer c1;
    private Customer c2;
    private Customer c3;
    private Customer c4;

    public DatasetInitialiser(
            CustomerService customerService,
            InsuranceCompanyService insuranceCompanyService,
            InsuranceService insuranceService,
            InvoiceService invoiceService,
            TreatmentService treatmentService,
            AddressRepository addressRepository,
            NameRepository nameRepository,
            DiscountStrategyRepository discountStrategyRepository,
            TreatmentMementoService treatmentMementoService,
            TreatmentCaretakerService treatmentCaretakerService
            ){
        this.customerService = customerService;
        this.insuranceCompanyService = insuranceCompanyService;
        this.insuranceService = insuranceService;
        this.invoiceService = invoiceService;
        this.treatmentService = treatmentService;
        this.addressRepository = addressRepository;
        this.nameRepository = nameRepository;
        this.discountStrategyRepository = discountStrategyRepository;
        this.treatmentCaretakerService = treatmentCaretakerService;
        this.treatmentMementoService = treatmentMementoService;
        this.initialise();
    }

    public void initialise(){
        createInsuranceCompanies();
        createTreatments();
        createInsurances();
        createCustomers();
        createInvoices();
    }

    private void createTreatments(){
        // Base treatments
        Treatment t1 = new Treatment("30", "Lichamelijk onderzoek", new BigDecimal("1"),
                "1", new BigDecimal("120"));
        Treatment t2 = new Treatment("45", "Consult", new BigDecimal("1"),
                "1", new BigDecimal("90"));
        Treatment t3 = new Treatment("54", "Hecting, gipsspalk, prothese", new BigDecimal("2"),
                "3", new BigDecimal("180"));

        // Premium treatments
        Treatment t4 = new Treatment("47", "Overleg met specialist", new BigDecimal("1"),
                "1", new BigDecimal("155"));
        Treatment t5 = new Treatment("34", "Bloedonderzoek", new BigDecimal("1"),
                "2", new BigDecimal("210"));
        Treatment t6 = new Treatment("35", "Urineonderzoek", new BigDecimal("1"),
                "2", new BigDecimal("210"));

        //Fysio treatments
        Treatment t7 = new Treatment("61", "Consult fysiotherapeut", new BigDecimal("1"),
                "1", new BigDecimal("90"));
        Treatment t8 = new Treatment("65", "Fysiotherapie in verband met een chronische aandoening", new BigDecimal("6"),
                "1", new BigDecimal("150"));
        Treatment t9 = new Treatment("66", "Fysiotherapie tijdens een ziekenhuisopname", new BigDecimal("2"),
                "1", new BigDecimal("150"));

        //Dental treatments
        Treatment t10 = new Treatment("81", "Controle", new BigDecimal("1"),
                "1", new BigDecimal("120"));
        Treatment t11 = new Treatment("82", "Vulling", new BigDecimal("1"),
                "1", new BigDecimal("160"));
        Treatment t12 = new Treatment("83", "Kroon", new BigDecimal("1"),
                "1", new BigDecimal("210"));

        treatmentService.createTreatment(t1);
        treatmentService.createTreatment(t2);
        treatmentService.createTreatment(t3);
        treatmentService.createTreatment(t4);
        treatmentService.createTreatment(t5);
        treatmentService.createTreatment(t6);
        treatmentService.createTreatment(t7);
        treatmentService.createTreatment(t8);
        treatmentService.createTreatment(t9);
        treatmentService.createTreatment(t10);
        treatmentService.createTreatment(t11);
        treatmentService.createTreatment(t12);
    }

    private void createInsuranceCompanies(){
        Address icAddress = new Address("Hogeschoollaan 1", "1324AC", "Breda");
        addressRepository.save(icAddress);

        ic = new InsuranceCompany("Avans Verzekeraars", icAddress,
                "12345678", "NL123456789B12", "SNS","info@avans-verzekeringen.nl");

        insuranceCompanyService.createInsuranceCompany(ic);
    }

    private void createInsurances(){

        DiscountStrategy d1 = new StandardDiscount();
        DiscountStrategy d2 = new NewCustomerDiscount();
        DiscountStrategy d3 = new ReturningCustomerDiscount();

        List<DiscountStrategy> discountStrategies = new ArrayList<DiscountStrategy>(){{
            add(d1);
            add(d2);
            add(d3);
        }};

        discountStrategyRepository.save(discountStrategies);

        i1 = new BasicInsurance("Basis verzekering", ic, d1);
        i2 = new SupplementaryInsurance("Aanvullende verzekering", ic, d1);
        i3 = new DentalPackageDecorator(new BasicInsurance("Basis verzekering", ic, d2));
        i4 = new FysioPackageDecorator(new SupplementaryInsurance("Aanvullende verzekering", ic, d3));

        insuranceService.createInsurance(i1);
        insuranceService.createInsurance(i2);
        insuranceService.createInsurance(i3);
        insuranceService.createInsurance(i4);

    }

    private void createCustomers(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1988);
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        Date dateRepresentation = cal.getTime();

        Address a1 = new Address("Straat 1", "5201HC", "Breda");
        Address a2 = new Address("Straat 2", "5202HC", "Breda");
        Address a3 = new Address("Straat 3", "5203HC", "Breda");
        Address a4 = new Address("Straat 4", "5204HC", "Breda");
        List<Address> al = new ArrayList<Address>(){{
            add(a1);
            add(a2);
            add(a3);
            add(a4);
        }};
        addressRepository.save(al);

        Name n1 = new Name("Jaap", "", "Jansen");
        Name n2 = new Name("Klaas", "van", "Ginder");
        Name n3 = new Name("Willem", "", "Smits");
        Name n4 = new Name("Pieter", "de", "Groot");
        List<Name> nl = new ArrayList<Name>(){{
            add(n1);
            add(n2);
            add(n3);
            add(n4);
        }};
        nameRepository.save(nl);

        c1 = new Customer("987654321", n1, a1,
                dateRepresentation, "0642585002", "jj@test.nl",
                Gender.MALE, i1, 50);
        c2 = new Customer("987654322", n2, a2,
                dateRepresentation, "0642585012", "kg@test.nl",
                Gender.MALE, i2, 0);
        c3 = new Customer("987654323", n3, a3,
                dateRepresentation, "0642585022", "ws@test.nl",
                Gender.MALE, ((InsurancePackageDecorator) i3).getInsurance(), 150);
        c4 = new Customer("987654324", n4, a4,
                dateRepresentation, "0642585032", "pg@test.nl",
                Gender.MALE, ((InsurancePackageDecorator) i4).getInsurance(), 500);
        customerService.createCustomer(c1);
        customerService.createCustomer(c2);
        customerService.createCustomer(c3);
        customerService.createCustomer(c4);
    }

    private void createInvoices(){
        Calendar ca1 = Calendar.getInstance();
        Calendar ca2 = Calendar.getInstance();
        Calendar ca3 = Calendar.getInstance();
        Calendar ca4 = Calendar.getInstance();

        ca1.add(Calendar.DAY_OF_MONTH, -4);
        ca2.add(Calendar.DAY_OF_MONTH, -3);
        ca3.add(Calendar.DAY_OF_MONTH, -2);
        ca4.add(Calendar.DAY_OF_MONTH, -1);
        Date d1 = ca1.getTime();
        Date d2 = ca2.getTime();
        Date d3 = ca3.getTime();
        Date d4 = ca4.getTime();

        ca1.add(Calendar.MONTH, 1);
        ca2.add(Calendar.MONTH, 1);
        ca3.add(Calendar.MONTH, 1);
        ca4.add(Calendar.MONTH, 1);
        Date ed1 = ca1.getTime();
        Date ed2 = ca2.getTime();
        Date ed3 = ca3.getTime();
        Date ed4 = ca4.getTime();

        Invoice i1 = new Invoice("Factuur 1", c1, d1, ed1);
        Invoice i2 = new Invoice("Factuur 2", c2, d2, ed2);
        Invoice i3 = new Invoice("Factuur 3", c3, d3, ed3);
        Invoice i4 = new Invoice("Factuur 4", c4, d4, ed4);

        i1.addTreatment(treatmentService.getTreatmentByCode("45"));
        i1.addTreatment(treatmentService.getTreatmentByCode("82"));

        i2.addTreatment(treatmentService.getTreatmentByCode("30"));
        i3.addTreatment(treatmentService.getTreatmentByCode("45"));
        i4.addTreatment(treatmentService.getTreatmentByCode("54"));

        invoiceService.createInvoice(i1);
        invoiceService.createInvoice(i2);
        invoiceService.createInvoice(i3);
        invoiceService.createInvoice(i4);


        TreatmentMemento tm1 = treatmentMementoService.createTrementMemento(new TreatmentMemento(treatmentService.getTreatmentByCode("45")));
        TreatmentMemento tm2 = treatmentMementoService.createTrementMemento(new TreatmentMemento(treatmentService.getTreatmentByCode("30")));

        TreatmentCaretaker caretaker = new TreatmentCaretaker();
        caretaker.addmemento(tm1);
        caretaker.addmemento(tm2);
        
    }
}
