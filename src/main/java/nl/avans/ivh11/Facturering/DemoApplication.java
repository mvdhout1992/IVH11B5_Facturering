package nl.avans.ivh11.Facturering;

import nl.avans.ivh11.Facturering.helpers.DatasetInitialiser;
import nl.avans.ivh11.Facturering.repository.*;
import nl.avans.ivh11.Facturering.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {

    @Autowired
    NameRepository nameRepository;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    TreatmentRepository treatmentRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    InsuranceRepository insuranceRepository;
    @Autowired
    InsuranceCompanyRepository insuranceCompanyRepository;
    @Autowired
    DiscountStrategyRepository discountStrategyRepository;
    @Autowired
    CaretakerRepository caretakerRepository;
    @Autowired
    MementoRepository mementoRepository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(){
		return (String... args) -> {
			System.out.println("CommandLineRunner running in the UnsplashApplication class...");
			TreatmentService ts = new TreatmentService(treatmentRepository);
			InsuranceService is = new InsuranceService(insuranceRepository, discountStrategyRepository, ts);
			new DatasetInitialiser(
                    new CustomerService(customerRepository, addressRepository, nameRepository, insuranceRepository, is),
                    new InsuranceCompanyService(insuranceCompanyRepository),
                    is,
                    new InvoiceService(invoiceRepository),
                    ts,
                    addressRepository,
                    nameRepository,
                    discountStrategyRepository,
                    new TreatmentMementoService(mementoRepository),
                    new TreatmentCaretakerService(caretakerRepository)
            );
		};
	}
}
